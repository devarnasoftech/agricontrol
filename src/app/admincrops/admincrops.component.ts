import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup,FormControl, Validators } from '@angular/forms';
import { AdminauthService } from '../service/adminauth.service';
import { first } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { LocalstorageService } from '../service/localstorage.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-admincrops',
  templateUrl: './admincrops.component.html',
  styleUrls: ['./admincrops.component.scss']
})
export class AdmincropsComponent implements OnInit {

  cropForm: FormGroup;
  allCrops$: any;
  allCropFamily$: any;
  filterData: any;
  isError = false;
  isModalError = false;
  errorMessage = '';
  modalErrorMessage = '';
  isSuccess = false;
  successMessage = '';
  addcropModal = 'none';
  cropModal = 'none';
  serverError$: any;
  editcolor = '#ffffff';
  isLoading = false;
  allLanguages$: any;

  constructor(
    private adminservice: AdminauthService,
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder,
    private storage: LocalstorageService,
    private translate: TranslateService,
    
  ) { }

  ngOnInit() {

    this.getAllCrops();
    this.getAllCropFamily();
    this.getAllLanguages();

    this.cropForm = this.formBuilder.group({
      /*name_en: ['', [Validators.required, Validators.maxLength(100)]],
      name_fr: ['', [Validators.required, Validators.maxLength(100)]],
      name_it: ['', [Validators.required, Validators.maxLength(100)]],
      name_de: ['', [Validators.required, Validators.maxLength(100)]],
      name_dp: ['', [Validators.required, Validators.maxLength(100)]],
      */
      family: ['', Validators.required],
      color: ['', Validators.required],
      cropId: ['', Validators.required],
      language: ['', Validators.required]
    });
    ///console.log(this.allLang);
  }
  
  /*for(let lang of  this.allLanguages$){
    var name = 'name_'+lang.symbol;
    console.log(name);
    ///this.cropForm.addControl(name, new FormControl('', Validators.required));
  }*/

  getAllLanguages() {
    this.adminservice.getAllLanguages().subscribe(data =>{ 
       this.allLanguages$ = data['data']; 
       
       for(let lang of  data['data']){
        var name = 'name_'+lang.symbol;
        this.cropForm.addControl(name, new FormControl('', Validators.required));
        //console.log(lang.symbol);
       }
    });
  }

  getAllCrops() {
    let defaultLanguage;
    const currentUser: any = this.storage.get('adminloginSession');
    defaultLanguage = { 'language': currentUser.language };
    this.adminservice.getAllCrops(defaultLanguage).pipe(first()).subscribe((data: any) => {
      this.allCrops$ = data['data'];
      this.filterData = data['data'];
      //console.log(data['data']);
    });
  }

  getAllCropFamily() {
    let defaultLanguage;
    const currentUser: any = this.storage.get('adminloginSession');
    defaultLanguage = { 'language': currentUser.language };
    this.adminservice.getAllCropFamily(defaultLanguage)
      .pipe(first())
      .subscribe((data: any) => {
        this.allCropFamily$ = data['data'];
      });
  }

  editCrop(crop_id,data) {
    this.cropModal = 'block';
    const currentUser: any = this.storage.get('adminloginSession');
    this.cropForm.reset();

    for(let lang of this.allLanguages$){
      var name = 'name_'+lang.symbol;
      this.cropForm.controls[name].setValue(data[name]);
    }
    this.cropForm.controls['cropId'].setValue(crop_id);
    this.cropForm.controls['family'].setValue(data.crop_family_id);
    this.cropForm.controls['color'].setValue(data.color);
    this.cropForm.controls['language'].setValue(currentUser.language);
    this.editcolor = data.color;
    //console.log(this.cropForm.value);
    /*const Input = { 'crop_id': crop_id, 'language': currentUser.language };
    this.adminservice.getCropDataById(Input).pipe(first()).subscribe((data: any) => {
      if (data.status) {
        const cropData = data.data;
        this.cropForm.setValue({
          name_de: cropData.name_de,
          name_fr: cropData.name_fr,
          name_en: cropData.name_en,
          name_it: cropData.name_it,
          family: cropData.crop_family_id,
          color: cropData.color,
          cropId: cropData.crop_id,
          language: currentUser.language
        });
        this.editcolor = cropData.color;
      }
    });*/
  }

  openaddCrop() {
    this.addcropModal = 'block';
    this.cropForm.reset();
    for(let lang of this.allLanguages$){
      var name = 'name_'+lang.symbol;
      //console.log(name);
      this.cropForm.controls[name].setValue('');
    }
    this.cropForm.controls['cropId'].setValue(0);
    this.cropForm.controls['family'].setValue('');
    this.cropForm.controls['color'].setValue('#eeeeee');
    this.cropForm.controls['language'].setValue('en');
    
    /*this.cropForm.setValue({
      name_en: "",
      name_fr: "",
      name_de: "",
      //name_it: "",
      //name_dp: "",
      family: "",
      color: "#eeeeee",
      cropId: 0,
      language: "en"
    });*/
    //console.log(this.cropForm.value);
  }

  updateCrop() {
    if (this.cropForm.invalid) {
      return;
    }
    this.cropForm.value.color = this.editcolor;

    this.adminservice.addCropData(this.cropForm.value).pipe(first()).subscribe((data: any) => {
      if (data.status) {
        this.isSuccess = true;
        //this.successMessage = data.message;
        this.isModalError = false;
        this.modalErrorMessage = '';
        this.translate.get(data.message).subscribe((result: string) => {
          this.successMessage = result;
        });
        this.closeCropModal();
        this.getAllCrops();
      } else {
        this.isModalError = true;
        /*this.serverError$ = data.message.split('<br>').map(function (item) {
          return item.trim();
        });
        this.modalErrorMessage = this.serverError$;
        */
        this.translate.get(data.message).subscribe((result: string) => {
          this.modalErrorMessage = result;
        });
      }
    });
  }

  addCrop() {
    if (this.cropForm.invalid) {
      return;
    }
    this.cropForm.value.color = this.editcolor;
    ///console.log(this.cropForm.value);
    this.adminservice.addCropData(this.cropForm.value).pipe(first()).subscribe((data: any) => {
      if (data.status) {
        this.isSuccess = true;
        //this.successMessage = data.message;
        this.translate.get(data.message).subscribe((result: string) => {
          this.successMessage = result;
        });
        this.isModalError = false;
        this.modalErrorMessage = '';
        this.closeaddCropModal();
        this.getAllCrops();
        
      } else {
        this.isModalError = true;
        /*this.serverError$ = data.message.split('<br>').map(function (item) {
          return item.trim();
        });
        this.modalErrorMessage = this.serverError$;
        */
        this.translate.get(data.message).subscribe((result: string) => {
          this.modalErrorMessage = result;
        });

      }
    });
  }

  deleteCrop(id, name){
    var isConfirm = confirm("Sind Sie sicher, dass Sie l�schen m�chten "+name+" Ernte");
    if(isConfirm){
      const Input = { 'crop_id': id };
      this.adminservice.deleteCrop(Input).pipe(first()).subscribe((data: any) => {
        if (data.status) {
          this.getAllCrops();
        } else {
          alert('Es ist ein Fehler aufgetreten. Bitte aktualisieren Sie die Seite und versuchen Sie es erneut.');
        }
      });
    }
  }

  closeCropModal() {
    this.cropForm.reset();
    this.isError = false;
    this.isModalError = false;
    this.cropModal = 'none';
    this.editcolor = '#ffffff';
  }

  closeaddCropModal() {
    this.cropForm.reset();
    this.isError = false;
    this.isModalError = false;
    this.addcropModal = 'none';
    this.editcolor = '#ffffff';
  }

  closemodal(modalName) {
    const modal = document.getElementById(modalName + 'modalId');
    const modalContent = document.getElementById(modalName + 'ContentId');
    const that = this;
    window.onclick = function (event) {
      if (event.target === modal && event.target !== modalContent) {
        if (modalName === 'crop') {
          that.cropForm.reset();
          that.closeCropModal();
        }
        modal.style.display = 'none';
      }
    };
  }

  search(term: string) {
    if (term === '') {
      this.filterData = this.allCrops$;
    } else {
      this.filterData = this.allCrops$.filter(x =>
        x.crop_name.trim().toLowerCase().includes(term.trim().toLowerCase())
      );
      if (this.filterData.length === 0) {
        this.filterData = this.allCrops$.filter(x =>
          x.crop_family_name.trim().toLowerCase().includes(term.trim().toLowerCase())
        );
      }
    }
  }

}

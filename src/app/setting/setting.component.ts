import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from '../service/common.service';
import { SettingService } from '../service/setting.service';

import { first, debounceTime } from 'rxjs/operators';
import { LocalstorageService } from '../service/localstorage.service';
import { NgxSpinnerService } from 'ngx-spinner';

import { AdminauthService } from '../service/adminauth.service';
import { AlertService } from '../service/alert.service';
import { TranslateService } from '@ngx-translate/core';
import { DeletealertService } from '../service/deletealert.service';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss']
})
export class SettingComponent implements OnInit {
  urls = new Array<string>();
  //To show image in preview
  detectFiles(event) {
    this.urls = [];
    let files = event.target.files;
    if (files) {
      for (let file of files) {
        let reader = new FileReader();
        reader.onload = (e: any) => {
          this.urls.push(e.target.result);
        }
        reader.readAsDataURL(file);
      }
    }
  }

  profileForm: FormGroup;
  changePasswordForm: FormGroup;
  addChildForm: FormGroup;
  serverError$: any;
  allLanguages$: any;
  allUsers$: any;
  filterData: any;
  subscriptionStartDate: string;
  subscriptionEndDate: string;
  currentUser$: any;
  isError = false;
  isErrorname = false;
  isErroremail = false;
  isErrorcompany = false;
  isErrorphone = false;
  isErrorstreet = false;
  isErrorpostalcode = false;
  isErrorcity = false;
  isErrorcountry = false;
  isErroroldpwd = false;
  isErrornewpwd = false;
  isErrorconfirmnewpwd = false;
  errorMessage = '';
  isSuccess = false;
  successMessage = '';
  isPasswordError = false;
  PassworderrorMessage = '';
  isPasswordSuccess = false;
  PasswordsuccessMessage = '';
  fileInput: File = null;
  filevalidationError = false;
  filevalidationErrorMsg = '';
  isLoading = false;
  startActivatedPlan = "";
  endActivatedPlan = "";
  isShowUpgradeMessage = false;

  isParentMessage = false;

  team_memberForm: FormGroup;

  isModalError = false;
  modalErrorMessage = '';
  addteam_memberModal = 'none';
  team_memberModal = 'none';
  isExpired = false;
  editcolor = '#ffffff';
  companyName = '';
  IsChildUser = false;
  showDeleteAccount = false;
  userPlan = '';
  userId = '';
  isAccountSuccess = false;
  isAccountError = false;
  errorAccountMessage = '';
  successAccountMessage = '';
  langArr : any;

  constructor(
    private auth: AuthService,
    private common: CommonService,
    private adminservice: AdminauthService,
    private alertService: AlertService,
    private setting: SettingService,
    private formBuilder: FormBuilder,
    private storage: LocalstorageService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private translate: TranslateService,
    private deletealertService: DeletealertService
  ) {
  }

  ngOnInit() {
    this.isExpired = this.storage.get('isExpired');
    this.profileForm = this.formBuilder.group({
      image: [''],
      name: ['', [Validators.required, Validators.maxLength(50)]],
      email: ['', [Validators.required, Validators.maxLength(50), Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
      company: ['', [Validators.required, Validators.maxLength(100)]],
      phone: ['', [Validators.required, Validators.maxLength(15)]],
      language: ['', [Validators.required]],
      streetName: ['', [Validators.required, Validators.maxLength(150)]],
      postalCode: ['', [Validators.required, Validators.maxLength(10), Validators.pattern('[0-9 ]*')]],
      city: ['', [Validators.required, Validators.maxLength(100), Validators.pattern('[A-zÀ-ÿ ]+')]],
      country: ['', [Validators.required, Validators.maxLength(100), Validators.pattern('[A-zÀ-ÿ ]+')]],
    });

    this.changePasswordForm = this.formBuilder.group({
      oldPassword: ['', [Validators.required, Validators.maxLength(25), Validators.minLength(6)]],
      password: ['', [Validators.required, Validators.maxLength(25), Validators.minLength(6)]],
      confirmPassword: ['', [Validators.required, Validators.maxLength(25), Validators.minLength(6)]],
    }, { validator: this.passwordMatchValidator('password', 'confirmPassword') });

    this.team_memberForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(100)]],
      email: ['', [Validators.required, Validators.maxLength(100), Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
      userID: ['', Validators.required],
      language: ['', Validators.required],
      company: ['', Validators.required]
    });

    this.getUserData();
    this.getAllLanguages();
    this.getAllteam_member();

    this.currentUser$ = this.storage.get('loginSession');
    // debugger;
    var message = localStorage.getItem("profileSetting");
    if (message != undefined && message != null && message == 'true') {
      this.isShowUpgradeMessage = true;
      localStorage.removeItem("profileSetting");
    }
  }
  Validationname() {
    this.isErrorname = true;
    //this.isErrorpwd = true;
  }
  ValidationErrorname() {
    this.isErrorname = false;
    // this.isErrorpwd = false;
  }

  Validationemail() {
    this.isErroremail = true;
    //this.isErrorpwd = true;
  }
  ValidationErroremail() {
    this.isErroremail = false;
    // this.isErrorpwd = false;
  }

  Validationcompany() {
    this.isErrorcompany = true;
    //this.isErrorpwd = true;
  }
  ValidationErrorcompany() {
    this.isErrorcompany = false;
    // this.isErrorpwd = false;
  }

  Validationphone() {
    this.isErrorphone = true;
    //this.isErrorpwd = true;
  }
  ValidationErrorphone() {
    this.isErrorphone = false;
    // this.isErrorpwd = false;
  }

  Validationstreet() {
    this.isErrorstreet = true;
    //this.isErrorpwd = true;
  }
  ValidationErrorstreet() {
    this.isErrorstreet = false;
    // this.isErrorpwd = false;
  }

  Validationpostalcode() {
    this.isErrorpostalcode = true;
    // this.isErrorpwd = true;
  }
  ValidationErrorpostalcode() {
    this.isErrorpostalcode = false;
    // this.isErrorpwd = false;
  }

  Validationcity() {
    this.isErrorcity = true;
    //this.isErrorpwd = true;
  }
  Validationerrorcity() {
    this.isErrorcity = false;
    // this.isErrorpwd = false;
  }

  Validationcountry() {
    this.isErrorcountry = true;
    //this.isErrorpwd = true;
  }
  Validationerrorcountry() {
    this.isErrorcountry = false;
    // this.isErrorpwd = false;
  }

  Validationoldpwd() {
    this.isErroroldpwd = true;
    //this.isErrorpwd = true;
  }
  Validationerroroldpwd() {
    this.isErroroldpwd = false;
    // this.isErrorpwd = false;
  }

  Validationnewpwd() {
    this.isErrornewpwd = true;
    //this.isErrorpwd = true;
  }
  Validationerrornewpwd() {
    this.isErrornewpwd = false;
    // this.isErrorpwd = false;
  }

  Validationconfirmnewpwd() {
    this.isErrorconfirmnewpwd = true;
    //this.isErrorpwd = true;
  }
  Validationerrorconfirmnewpwd() {
    this.isErrorconfirmnewpwd = false;
    // this.isErrorpwd = false;
  }

  passwordMatchValidator(password: string, confirmPassword: string) {
    return (group: FormGroup): { [key: string]: any } => {
      const f = group.controls[password];
      const t = group.controls[confirmPassword];
      if (f.value && t.value && f.value.length > 5 && t.value.length > 5 && f.value !== t.value) {
        var message = "";
        this.translate.get("password_not_match").subscribe((result: string) => {
          message = result;
        });
        return {
          passwords: message
        };
      }
      return {};
    };
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  checkPhone(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    const inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode !== 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  onFileChanged(event) {
    this.fileInput = <File>event.target.files[0];
    const fileExtension = this.fileInput.name.split('.').pop();
    const allowedExtensions = /(\jpg|\jpeg|\png|\gif)$/i;
    if (!allowedExtensions.exec(fileExtension.toLowerCase())) {
      this.translate.get("valid_file_extenstion").subscribe((result: string) => {
        this.filevalidationErrorMsg = result;
      });
      this.filevalidationError = true;
      //this.filevalidationErrorMsg = 'Please upload file having extensions .jpeg/.jpg/.png/.gif only.';
      this.profileForm.get('image').reset();
      return false;
    } else {
      const fileSize = this.fileInput.size / 1024 / 1024; // in MB
      if (fileSize > 3) {
        this.translate.get("max_file_size_message").subscribe((result: string) => {
          this.filevalidationErrorMsg = result;
        });
        this.filevalidationError = true;
        this.profileForm.get('image').reset();
        return false;
      } else {
        window.URL = window.URL || (window as any).webkitURL;
        const reader = new FileReader();
        const img = new Image();
        img.src = window.URL.createObjectURL(this.fileInput);
        reader.readAsDataURL(this.fileInput);
        reader.onload = () => {
          const width = img.naturalWidth;
          const height = img.naturalHeight;
          window.URL.revokeObjectURL(img.src);
          if (width > 2000 && height > 2000) {
            this.filevalidationError = true;
            this.translate.get("max_file_size_message").subscribe((result: string) => {
              this.filevalidationErrorMsg = result;
            });
            //this.filevalidationErrorMsg = 'photo should be less than 2000 x 2000 size.';
            this.profileForm.get('image').reset();
            return false;
          } else {
            this.filevalidationError = false;
            this.filevalidationErrorMsg = '';
            return true;
          }
        };
      }
    }
  }

  getUserData() {
    this.setting.getUserData().pipe(first()).subscribe((data: any) => {
      if (data.status) {
        const userData = data.data;
        //console.log(userData);
        debounceTime;
        if (userData.is_parent == 1) { this.isParentMessage = true; }
        //debugger;
        this.userPlan = userData.plan;
        this.userId = userData.ac_userId;
        if (userData.plan == "Trial") {
          this.translate.get("trial_start").subscribe((result: string) => {
            this.startActivatedPlan = result;
          });
          this.translate.get("trial_end").subscribe((result: string) => {
            this.endActivatedPlan = result;
          });
        } else {
          this.translate.get("subscription_start").subscribe((result: string) => {
            this.startActivatedPlan = result;
          });
          this.translate.get("subscription_end").subscribe((result: string) => {
            this.endActivatedPlan = result;
          });
        }
        this.profileForm.setValue({
          image: '',
          name: userData.name,
          email: userData.email,
          company: userData.company,
          phone: userData.phone,
          language: userData.language,
          streetName: userData.street_name,
          postalCode: userData.postal_code,
          city: userData.city,
          country: userData.country
        });
        if (userData.is_parent != 1) {
          this.IsChildUser = true;
          this.profileForm.controls['email'].disable();
          this.profileForm.controls['company'].disable();
          this.profileForm.controls['phone'].disable();
          this.profileForm.controls['streetName'].disable();
          this.profileForm.controls['postalCode'].disable();
          this.profileForm.controls['city'].disable();
          this.profileForm.controls['country'].disable();
        }else{
          this.showDeleteAccount = true;
        }
        this.companyName = userData.company;
        let startDate = new Date(userData.subscription_start.replace(" ", "T"));
        this.subscriptionStartDate = (startDate.getDate()) + '.' + ("0" + (Number.parseInt(startDate.getMonth().toString()) + 1)).slice(-2) + '.' + (startDate.getFullYear());
        let endDate = new Date(userData.subscription_end.replace(" ", "T"));
        this.subscriptionEndDate = (endDate.getDate()) + '.' + ("0" + (Number.parseInt(endDate.getMonth().toString()) + 1)).slice(-2) + '.' + (endDate.getFullYear());
      }
    });
  }

  addTeam_member() {
    if (this.team_memberForm.invalid) {
      return;
    }
    this.team_memberForm.value.color = this.editcolor;

    this.setting.addTeam_memberData(this.team_memberForm.value).pipe(first()).subscribe((data: any) => {
      if (data.status) {
        this.isSuccess = true;
        this.isModalError = false;
        this.modalErrorMessage = '';
        /*this.translate.get("mail_sent").subscribe((result: string) => {
          this.successMessage = result;
        });*/
        
        this.closeaddTeam_memberModal();
        this.getAllteam_member();
      } else {
        this.isSuccess = false;
        this.isModalError = true;
        /*this.serverError$ = data.message.split('<br>').map(function (item) {
          return item.trim();
        });*/

        /*if (data.message == "Email Already Registerd") {
          this.translate.get("email_already").subscribe((result: string) => {
            this.modalErrorMessage = result;
          });
        } else {
          this.translate.get("error_something_wrong").subscribe((result: string) => {
            this.modalErrorMessage = result;
          });
        }*/
        //this.modalErrorMessage = this.serverError$;
      }
      if(data.message && data.message != ''){
        this.translate.get(data.message).subscribe((result: string) => {
          if(this.isSuccess == true){
            this.successMessage = result;
          }else{
            this.modalErrorMessage = result;
          }
        });
      }

    });
  }

  openaddTeam_member() {
    console.log(this.currentUser$);
    this.addteam_memberModal = 'block';
    this.team_memberForm.reset();
    this.team_memberForm.setValue({
      name: "",
      email: "",
      userID: "1",
      language: this.translate.defaultLang,
      company: this.companyName
    });
  }

  closeaddTeam_memberModal() {
    this.team_memberForm.reset();
    this.isError = false;
    this.addteam_memberModal = 'none';

  }

  closemodal(modalName) {
    const modal = document.getElementById(modalName + 'modalId');
    const modalContent = document.getElementById(modalName + 'ContentId');
    const that = this;
    window.onclick = function (event) {
      if (event.target === modal && event.target !== modalContent) {
        if (modalName === 'team_member') {
          that.team_memberForm.reset();

        }
        modal.style.display = 'none';
      }
    };
  }

  deleteUser(user_id) {
    const that = this;
    this.translate.get("user_delete_confirm").subscribe((result: string) => {
      const confirmMsg = result;
    });
    const Input = { 'user_id': user_id };
    that.setting.deleteUser(Input).pipe(first()).subscribe((data: any) => {
      
      if (data.status) {
        that.isSuccess = true;
        that.isError = false;
        /*this.translate.get("user_delete").subscribe((result: string) => {
          that.successMessage = result;
        });*/
        //that.successMessage = data.message;
        that.errorMessage = '';
        that.getAllteam_member();
      } else {
        that.isSuccess = false;
        that.isError = true;
        that.successMessage = '';
        /*this.translate.get("error_something_wrong").subscribe((result: string) => {
          that.errorMessage = result;
        });*/
        //that.errorMessage = data.message;
      }
      if(data.message && data.message != ''){
        this.translate.get(data.message).subscribe((result: string) => {
          if(that.isSuccess == true){
            that.successMessage = result;
          }else{
            that.errorMessage = result;
          }
        });
      }

    });
  }

  getAllteam_member() {
    this.setting.getAllteam_member().pipe(first()).subscribe((data: any) => {
      this.allUsers$ = data['data'];
      this.filterData = data['data'];
    });
  }

  getAllLanguages() {
    this.common.getAllLanguages().subscribe((data:any)=>{

      this.langArr = [];
      var jsonArr;
      for(let x of data['data']) {
        if (x.is_publish == 1) {
          //console.log(x);
          jsonArr = {};
          jsonArr['ac_languageId'] = x.ac_languageId;
          jsonArr['name'] = x.name;
          jsonArr['symbol'] = x.symbol;
          jsonArr['is_publish'] = x.is_publish;
          this.langArr.push(jsonArr);
        }
      }
      this.allLanguages$ = this.langArr;
      //data => this.allLanguages$ = data['data'];
    });
  }

  SubmitProfile() {
    if (this.profileForm.invalid) {
      return;
    }
    if (this.IsChildUser) {
      this.profileForm.controls['email'].enable();
      this.profileForm.controls['company'].enable();
      this.profileForm.controls['phone'].enable();
      this.profileForm.controls['streetName'].enable();
      this.profileForm.controls['postalCode'].enable();
      this.profileForm.controls['city'].enable();
      this.profileForm.controls['country'].enable();
    }
    //this.spinner.show();
    this.isLoading = true;
    this.setting.updateProfile(this.profileForm.value).pipe(first()).subscribe((data: any) => {
      if (data.status) {
        //this.spinner.hide();
        this.isLoading = false;
        if (this.fileInput !== null) {
          this.UploadFile(data);
        } else {
          this.isSuccess = true;
          this.isError = false;
          ///debugger;
          //this.successMessage = data.message;
          setTimeout(() => { this.UpdateSession(); }, 3000);
        }
      } else {
        this.isLoading = false;
        this.isError = true;
        this.isSuccess = false;
        // this.serverError$ = data.message.split('<br>').map(function (item) { return item.trim(); });
        // this.errorMessage = this.serverError$;
        /*this.translate.get("error_something_wrong").subscribe((result: string) => {
          this.errorMessage = result;
        });*/
      }

      if(data.message && data.message != ''){
        this.translate.get(data.message).subscribe((result: string) => {
          if(this.isSuccess == true){
            this.successMessage = result;
          }else{
            this.errorMessage = result;
          }
        }); 
      }
      
      this.getUserData();
    });
  }

  UploadFile(data) {
    //this.spinner.show();
    this.isLoading = true;
    const uploadData = new FormData();
    uploadData.append('image', this.fileInput, this.fileInput.name);
    this.setting.addImage(uploadData).pipe(first()).subscribe((res: any) => {
      if (res.status) {
        this.isSuccess = true;
        this.isError = false;
        /*this.translate.get("profile_update_success").subscribe((result: string) => {
          this.successMessage = result;
        });*/
        //this.successMessage = res.message;
        
        this.currentUser$.profile_picture = res.data;
        setTimeout(() => { this.UpdateSession(); }, 2000);
      } else {
        this.isError = true;
        this.isSuccess = false;
        /*this.serverError$ = res.message.split('<br>').map(function (item) { return item.trim(); });
        this.translate.get("error_something_wrong").subscribe((result: string) => {
          this.errorMessage = result;
        });*/
        //this.errorMessage = this.serverError$;
      }

      if(res.message && res.message != ''){
        this.translate.get(res.message).subscribe((result: string) => {
          if(this.isSuccess == true){
            this.successMessage = result;
          }else{
            this.errorMessage = result;
          }
        });
      }

      this.fileInput = null;
      this.profileForm.get('image').reset();
      //this.spinner.hide();
      this.isLoading = false;
    });
  }

  SubmitChangePassword() {
    if (this.changePasswordForm.invalid) {
      return;
    }
    //this.spinner.show();
    this.isLoading = true;
    this.setting.changePassword(this.changePasswordForm.value).pipe(first()).subscribe((data: any) => {
      if (data.status) {
        this.isPasswordSuccess = true;
        this.isPasswordError = false;
        /*this.translate.get("reset_password_success").subscribe((result: string) => {
          this.PasswordsuccessMessage = result;
        });*/
        //this.PasswordsuccessMessage = data.message;
      } else {
        this.isPasswordError = true;
        this.isPasswordSuccess = false;
        // this.serverError$ = data.message.split('<br>').map(function (item) {
        //   return item.trim();
        // });
        //this.PassworderrorMessage = this.serverError$;
      }
      if(data.message && data.message != ''){
        this.translate.get(data.message).subscribe((result: string) => {
          if(this.isPasswordSuccess == true){
            this.PasswordsuccessMessage = result;
          }else{
            this.PassworderrorMessage = result;  
          }
        });
      }
      //this.spinner.hide();
      this.isLoading = false;
    });
  }

  UpdateSession() {
    this.currentUser$.company = this.profileForm.controls.company.value;
    this.currentUser$.language = this.profileForm.controls.language.value;
    for (var i = 0; i < this.allLanguages$.length; i++) {
      if (this.allLanguages$[i]['ac_languageId'] == this.currentUser$.language) {
        this.currentUser$.languageKey = this.allLanguages$[i]['symbol'];
        this.currentUser$.Name = this.allLanguages$[i]['name'];
      }
    }
    //this.currentUser$.languageKey = this.allLanguages$[this.currentUser$.language - 1].symbol;
    //this.currentUser$.Name = this.allLanguages$[this.currentUser$.language - 1].name;
    //console.log("this.allLanguages$ ",this.allLanguages$ );
    this.storage.set('loginSession', this.currentUser$);
    // this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
    // this.router.navigate(['/settings']));
    if (this.isShowUpgradeMessage) {
      localStorage.setItem("openUpgradeModel", 'true');
    }
    window.location.reload();
  }

  DeleteAccount(){
    //debugger;
    ///console.log(this.userPlan);
    if (this.userPlan == "Trial") {
      var text_title = 'trial_delete_text';
    }else{
      var text_title = 'mainuser_delete_text';
    }
    let confirmMsg;
    this.translate.get(text_title).subscribe((result: string) => {
      confirmMsg = result;
    });

    const Input = { 'user_id': this.userId,'user_plan': this.userPlan};
    const that = this;
    let yesText = 'yes_delete';
    let noText = 'dont_delete';
    
    that.deletealertService.confirmThis('Confirmation', confirmMsg,yesText,noText, function () {
      //debugger;
      that.setting.deleteUserAccount(Input).pipe(first()).subscribe((data: any) => {
        if (data.status) {
          that.isAccountSuccess = true;
          that.isAccountError = false;
          that.errorAccountMessage = '';

          setTimeout(() => { that.logOut(); }, 3000);
        } else {
          that.isAccountSuccess = false;
          that.isAccountError = true;
          that.successAccountMessage = '';
        }
        if(data.message && data.message != ''){
          that.translate.get(data.message).subscribe((result: string) => {
            if(that.isAccountSuccess == true){
              that.successAccountMessage = result;
            }else{
              that.errorAccountMessage = result;
            }
          });
        }
      });
    }, function () {
      console.log('No');
    });
  }

  logOut() {
    const currentUser = this.storage.get('loginSession');
    this.auth.logOut(currentUser)
      .pipe(first())
      .subscribe((data: any) => {
        if (data.status) {
          const rememberMeStatus = this.storage.get('rememberMe');
          this.storage.remove('loginSession');
          this.storage.remove('currentLanguage');
          localStorage.clear();
          sessionStorage.clear();
          this.storage.removeAll();
          this.storage.set("rememberMe", rememberMeStatus);
          
          this.router.navigate(['login']);

        }
      });
  }
  
}

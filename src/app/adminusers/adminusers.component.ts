import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AdminauthService } from '../service/adminauth.service';
import { first } from 'rxjs/operators';
import { AlertService } from '../service/alert.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonService } from '../service/common.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-adminusers',
  templateUrl: './adminusers.component.html',
  styleUrls: ['./adminusers.component.scss']
})
export class AdminusersComponent implements OnInit {

  userForm: FormGroup;
  subscriptionForm: FormGroup;
  allUsers$: any;
  filterData: any;
  isError = false;
  isModalError = false;
  errorMessage = '';
  modalErrorMessage = '';
  isSuccess = false;
  successMessage = '';
  userModal = 'none';
  serverError$: any;
  upgradeUserEmail = '';
  allSubscriptions$: any;
  selectedPlanId = 0;
  historyData: any;
  isLoading = false;

  constructor(
    private adminservice: AdminauthService,
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private common: CommonService,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.userForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(50)]],
      company: ['', [Validators.required, Validators.maxLength(100)]],
      email: ['', [Validators.required, Validators.maxLength(50), Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
      phone: ['', [Validators.required, Validators.maxLength(15)]],
      streetName: ['', [Validators.required, Validators.maxLength(150)]],
      postalCode: ['', [Validators.required, Validators.maxLength(10), Validators.pattern('[0-9 ]*')]],
      city: ['', [Validators.required, Validators.maxLength(100), Validators.pattern('[A-zÀ-ÿ ]+')]],
      country: ['', [Validators.required, Validators.maxLength(100), Validators.pattern('[A-zÀ-ÿ ]+')]],
      userId: ['', Validators.required]
    });

    this.subscriptionForm = this.formBuilder.group({
      method: ['', [Validators.required, Validators.maxLength(50)]],
      comment: ['', [Validators.maxLength(150)]],
      userId: ['', Validators.required],
      planId: ['', Validators.required]
    });

    this.getAllUsers();
    this.getSubscription();
  }

  getSubscription() {
    this.common.getAllSubscriptions().subscribe((data: any) => {
      if (data.status) {
        this.allSubscriptions$ = data['data'];
      } else {
        /*this.serverError$ = data.message.split('<br>').map(function (item) {
          return item.trim();
        });*/
        this.translate.get(data.message).subscribe((result: string) => {
          this.serverError$ = result;
        });
      }
    });
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  checkPhone(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    const inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode !== 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  getAllUsers() {
    this.adminservice.getAllUsers().pipe(first()).subscribe((data: any) => {
      this.allUsers$ = data['data'];
      this.allUsers$.forEach(element => {
        element.IsExpired = this.checkSubscriptionEnd(element.subscription_end);
      });
      this.filterData = data['data'];
      console.log(data);
    });
  }

  checkSubscriptionEnd(date){
    var subscriptionEnd = new Date(date);
    if(subscriptionEnd < new Date()){
      return true;
    }
    return false;
  }

  search(term: string) {
    if (term === '') {
      this.filterData = this.allUsers$;
    } else {
      this.filterData = this.allUsers$.filter(x =>
        x.name.trim().toLowerCase().includes(term.trim().toLowerCase())
      );
      if (this.filterData.length === 0) {
        this.filterData = this.allUsers$.filter(x =>
          x.company.trim().toLowerCase().includes(term.trim().toLowerCase())
        );
        if (this.filterData.length === 0) {
          this.filterData = this.allUsers$.filter(x =>
            x.email.trim().toLowerCase().includes(term.trim().toLowerCase())
          );
          if (this.filterData.length === 0) {
            this.filterData = this.allUsers$.filter(x =>
              x.street_name.trim().toLowerCase().includes(term.trim().toLowerCase())
            );
            if (this.filterData.length === 0) {
              this.filterData = this.allUsers$.filter(x =>
                x.postal_code.trim().toLowerCase().includes(term.trim().toLowerCase())
              );
              if (this.filterData.length === 0) {
                this.filterData = this.allUsers$.filter(x =>
                  x.city.trim().toLowerCase().includes(term.trim().toLowerCase())
                );
                if (this.filterData.length === 0) {
                  this.filterData = this.allUsers$.filter(x =>
                    x.created_at.trim().toLowerCase().includes(term.trim().toLowerCase())
                  );
                }
              }
            }
          }
        }
      }
    }
  }

  changeActivation(name, subscriptionId, status) {
    const that = this;
    var activate = status == 0 ? 'aktivieren Sie' : 'Deaktivieren';
    const confirmMsg = 'Möchtest du wirklich' + activate + ' zum ' + name;
    that.alertService.confirmThis('Confirmation', confirmMsg, function () {
      that.spinner.show();
      const Input = {'status': status, 'subscriptionId': subscriptionId };
      that.adminservice.updateActivation(Input).pipe(first()).subscribe((data: any) => {
        if (data.status) {
          that.isSuccess = true;
          that.isError = false;
          //that.successMessage = data.message;
          that.errorMessage = '';
          that.spinner.hide();
          that.getAllUsers();
        } else {
          that.isSuccess = false;
          that.isError = true;
          that.successMessage = '';
          //that.errorMessage = data.message;
          that.spinner.hide();
        }
        if(data.message && data.message != ''){
          that.translate.get(data.message).subscribe((result: string) => {
            if(that.isSuccess == true){
              that.successMessage = result;
            }else{
              that.errorMessage = result;
            }
          }); 
        }

      });
    }, function () {
      console.log('No');
    });
  }

  deleteUser(user_id) {
    const that = this;
    const confirmMsg = 'Möchten Sie diesen Benutzer wirklich löschen?';
    that.alertService.confirmThis('Confirmation', confirmMsg, function () {
      that.spinner.show();
      const Input = { 'user_id': user_id };
      that.adminservice.deleteUser(Input).pipe(first()).subscribe((data: any) => {
        if (data.status) {
          that.isSuccess = true;
          that.isError = false;
          //that.successMessage = data.message;
          that.errorMessage = '';
          that.spinner.hide();
          that.getAllUsers();
        } else {
          that.isSuccess = false;
          that.isError = true;
          that.successMessage = '';
          //that.errorMessage = data.message;
          that.spinner.hide();
        }
        if(data.message && data.message != ''){
          that.translate.get(data.message).subscribe((result: string) => {
            if(that.isSuccess == true){
              that.successMessage = result;
            }else{
              that.errorMessage = result;
            }
          }); 
        }
        
      });
    }, function () {
      console.log('No');
    });
  }

  selectedPlan(id) {
    this.selectedPlanId = id;
    this.subscriptionForm.controls['planId'].setValue(id);
  }

  edituser(user_id) {
    this.userModal = 'block';
    this.userForm.reset();
    const Input = { 'user_id': user_id };
    this.adminservice.getUserDataById(Input).pipe(first()).subscribe((data: any) => {
      if (data.status) {
        const userData = data.data;
        this.userForm.setValue({
          name: userData.name,
          company: userData.company,
          email: userData.email,
          phone: userData.phone,
          streetName: userData.street_name,
          postalCode: userData.postal_code,
          city: userData.city,
          country: userData.country,
          userId: userData.ac_userId
        });
      }
    });
  }

  upgradePlan(user_id, email) {
    this.subscriptionForm.reset();
    this.upgradeUserEmail = email;
    this.selectedPlanId = 0;
    this.subscriptionForm.setValue({
      method: 'Cash',
      comment: '',
      userId: user_id,
      planId: ''
    });
  }

  planHistory(user_id, email) {
    this.subscriptionForm.reset();
    this.upgradeUserEmail = email;
    this.historyData = [];
    let userID = { 'user_id': user_id };
    this.adminservice.getUserHistory(userID).subscribe((data: any) => {
      this.historyData = data["data"];
    });
  }

  upgradePlanSubmit() {
    if (this.subscriptionForm.invalid) {
      return;
    }

    this.adminservice.upgradeUserPlan(this.subscriptionForm.value).pipe(first()).subscribe((data: any) => {
      window.location.reload();
    });
  }

  updateUser() {
    if (this.userForm.invalid) {
      return;
    }

    this.adminservice.updateUseData(this.userForm.value).pipe(first()).subscribe((data: any) => {
      if (data.status) {
        this.isSuccess = true;
        //this.successMessage = data.message;
        this.isModalError = false;
        this.modalErrorMessage = '';
        this.closeUserModal();
        this.getAllUsers();
        this.translate.get(data.message).subscribe((result: string) => {
          this.successMessage = result;
        });
      } else {
        this.isModalError = true;
        /*this.serverError$ = data.message.split('<br>').map(function (item) {
          return item.trim();
        });
        this.modalErrorMessage = this.serverError$;*/
        this.translate.get(data.message).subscribe((result: string) => {
          this.modalErrorMessage = result;
        });
      }
    });
  }

  closeUserModal() {
    this.userForm.reset();
    this.isError = false;
    this.isModalError = false;
    this.userModal = 'none';
  }

  closemodal(modalName) {
    const modal = document.getElementById(modalName + 'modalId');
    const modalContent = document.getElementById(modalName + 'ContentId');
    const that = this;
    window.onclick = function (event) {
      if (event.target === modal && event.target !== modalContent) {
        if (modalName === 'user') {
          that.userForm.reset();
          that.closeUserModal();
        }
        modal.style.display = 'none';
      }
    };
  }

  exportUserList() {
    //this.spinner.show();
    this.isLoading = true;
    this.adminservice.exportUserList().pipe(first()).subscribe((data: any) => {
      if (data.status) {
        const file_path = data.data;
        const a = document.createElement('a');
        a.href = file_path;
        a.target = '_blank';
        a.download = '';
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
        this.isLoading = false;
        //this.spinner.hide();
      } else {
        this.isSuccess = false;
        this.isError = true;
        this.successMessage = '';
        //this.errorMessage = data.message;
        this.translate.get(data.message).subscribe((result: string) => {
          this.errorMessage = result;
        });
        //this.spinner.hide();
        this.isLoading = false;
      }
    });
  }
  
  exportcsvUserList() {
    //this.spinner.show();
    this.isLoading = true;
    this.adminservice.getAllUsersCsv().pipe(first()).subscribe((data: any) => {
      const replacer = (key, value) => value === null ? '' : value; // specify how you want to handle null values here
      const header = Object.keys(data.data[0]);
      let csv = data.data.map(row => header.map(fieldName => JSON.stringify(row[fieldName], replacer)).join(','));
      csv.unshift(header.join(','));
      let csvArray = csv.join('\r\n');
      var a = document.createElement('a');
      var blob = new Blob([csvArray], { type: 'text/csv' }),
        url = window.URL.createObjectURL(blob);
      a.href = url;
      a.download = "Agri Control Users List.csv";
      a.click();
      window.URL.revokeObjectURL(url);
      a.remove();
      this.isLoading = false;
    });
  }
}

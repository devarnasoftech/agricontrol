import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../service/auth.service';
import { LocalstorageService } from '../service/localstorage.service';
import { first } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-confirmemail',
  templateUrl: './confirmemail.component.html',
  styleUrls: ['./confirmemail.component.scss']
})
export class ConfirmEmailComponent implements OnInit {

  emailParam: string;
  invalidConfirm = false;
  tokenSuccess = false;
  serverError$: any;

  constructor(
    private auth: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private storage: LocalstorageService,
    private translate: TranslateService
    ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.emailParam = params.get('emailToken');
    });

    this.checkToken(this.emailParam);
  }

  checkToken(emailparameter) {
    if (emailparameter) {
      this.auth.confirmEmail(emailparameter)
      .pipe(first())
      .subscribe( (data: any) => {
        if (data.status) {
          this.tokenSuccess = true;
          this.invalidConfirm = false;
          this.storage.set('loginSession', data.data);
          this.storage.set('isExpired', false);
          let session = this.storage.get("loginSession");
          if (session && session.languageKey) {
            this.translate.setDefaultLang(session.languageKey);
            this.translate.setDefaultLang(session.languageKey);
            this.storage.set("defaultLanguage", session.languageKey);
          }
          setTimeout(this.navigateToRegisterFields.bind(this), 1500);
        } else {
          this.tokenSuccess = false;
          this.serverError$ = data.message.split('<br>').map(function(item) {
            return item.trim();
          });
          this.invalidConfirm = true;
        }
      });
    }
  }

  navigateToRegisterFields() {
    this.router.navigate(['/dashboard']);
  }

}

import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';
import { first } from 'rxjs/operators';
import { DashboardService } from '../service/dashboard.service';
import { TransferserviceService } from '../service/transferservice.service';
import * as jspdf from 'jspdf';
import 'jspdf-autotable';
import html2canvas from 'html2canvas';
import { DatePipe } from '@angular/common';
import { TitleCasePipe } from '@angular/common';

@Component({
  selector: 'app-graphical-export',
  templateUrl: './graphical-export.component.html',
  styleUrls: ['./graphical-export.component.scss']
})
export class GraphicalExportComponent implements OnInit {

  data: any;
  dashboardData$: any;
  fieldCultures$: any;
  math: any;
  isLoading = false;

  monthArr = [
    {
      'month': 'January',
      'symbol': 'Jan',
    },
    {
      'month': 'Febuary',
      'symbol': 'Feb',
    },
    {
      'month': 'March',
      'symbol': 'Mar',
    },
    {
      'month': 'April',
      'symbol': 'Apr',
    },
    {
      'month': 'May',
      'symbol': 'May',
    },
    {
      'month': 'June',
      'symbol': 'June',
    },
    {
      'month': 'July',
      'symbol': 'July',
    },
    {
      'month': 'August',
      'symbol': 'Aug',
    },
    {
      'month': 'September',
      'symbol': 'Sept',
    },
    {
      'month': 'October',
      'symbol': 'Oct',
    },
    {
      'month': 'November',
      'symbol': 'Nov',
    },
    {
      'month': 'December',
      'symbol': 'Dec',
    }
  ];

  constructor(
    private location: Location,
    private spinner: NgxSpinnerService,
    private transfereService: TransferserviceService,
    private dashboard: DashboardService,
    private titlecasePipe: TitleCasePipe,
    private datePipe: DatePipe
    ) {
      this.math = Math;
     }

  ngOnInit() {
    this.data = this.transfereService.getinputData();

    if (this.data) {
      this.generateExportDataPdf();
    } else {
      this.location.back();
    }
  }

  generateExportDataPdf() {
    //this.spinner.show();
    this.isLoading = true;
    this.dashboard.getchartExportData(this.data)
      .pipe(first())
      .subscribe( (data: any) => {
        if (data.status) {
          //this.spinner.hide();
          this.isLoading = false;
          this.dashboardData$ = data.data.export_field_data;
          this.fieldCultures$ = data.data.export_data;
          this.createPdfAndDownload();
        } else {
          //this.spinner.hide();
          this.isLoading = false;
          this.location.back();
        }
      });
  }

  getChartFieldDivHeight(fieldMaxCount) {
    const count = parseInt(fieldMaxCount, 10);
    if (count > 3) {
      const styles = {
        'height': fieldMaxCount * 20 + 'px'
      };
      return styles;
    } else {
      const styles = {
        'height': '75px'
      };
      return styles;
    }
  }

  checkShowMonthOnField(columnindex) {
    // const start = new Date(this.cultureMinDateObj);
    // const startMonth = start.getMonth();
    // const startDate = start.getDate();
    // if (columnindex === 0) {
    //   this.fullViewMonthShow = this.monthArr[startMonth].symbol;
    //   this.fullViewMonthNo = startMonth;
    // }
    // start.setDate(start.getDate() + columnindex);
    // const newMonth = start.getMonth();
    // if (this.fullViewMonthNo !== newMonth || (columnindex === 0 && startDate <= 15)) {
    //   this.fullViewMonthShow = this.monthArr[newMonth].symbol;
    //   this.fullViewMonthNo = newMonth;
      return true;
    // } else {
    //   return false;
    // }
  }

  createPdfAndDownload() {
    //this.spinner.show();
    this.isLoading = true;
    const doc = new jspdf('p', 'mm', 'a4');
    doc.rect(4, 4, doc.internal.pageSize.width - 8, doc.internal.pageSize.height - 8, 'S');
    doc.setFontSize(12);
    doc.setFontStyle('normal');

    const MonthWisehtmlData = document.getElementById('pdfHtml');
    html2canvas(MonthWisehtmlData, {
      scale: 5
    }).then(canvas1 => {
      const imgWidth = 208;
      const pageHeight = 295;
      const imgHeight = canvas1.height * imgWidth / canvas1.width;
      const heightLeft = imgHeight;

      const contentDataURL = canvas1.toDataURL('image/png');
      const position = 65;
      doc.addImage(contentDataURL, 'PNG', 8, position, imgWidth - 20, imgHeight);

      const fieldName = 'text';
      const fileName = 'Report-' + fieldName + '-' + '.pdf';

      doc.save(fileName);
      //this.spinner.hide();
      this.isLoading = false;
    });
    this.location.back();
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphicalExportComponent } from './graphical-export.component';

describe('GraphicalExportComponent', () => {
  let component: GraphicalExportComponent;
  let fixture: ComponentFixture<GraphicalExportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphicalExportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphicalExportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

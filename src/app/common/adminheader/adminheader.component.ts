import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject, timer, Subscription } from 'rxjs';
import { takeUntil, take, first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AdminauthService } from '../../service/adminauth.service';
import { environment } from '../../../environments/environment';
import { LocalstorageService } from '../../service/localstorage.service';

@Component({
  selector: 'app-adminheader',
  templateUrl: './adminheader.component.html',
  styleUrls: ['./adminheader.component.scss']
})
export class AdminheaderComponent implements OnDestroy, OnInit {

  apiSiteUrl = environment.apiSiteUrl;
  currentUserCompanyName: string;
  currentUserPicture = 'assets/images/profile_picture/user.png';
  minutes = 0;
  seconds = 0;
  endTime = 1;

  unsubscribe$: Subject<void> = new Subject();
  timerSubscription: Subscription;

  constructor(
    private adminservice: AdminauthService,
    private storage: LocalstorageService,
    private router: Router
    ) { }

  ngOnInit() {
    const currentUser: any = this.storage.get('adminloginSession');
    this.currentUserCompanyName = currentUser.company;
    this.currentUserPicture = (currentUser.profile_picture !== null) ? this.apiSiteUrl + 'uploads/profile_picture/' + currentUser.profile_picture : 'assets/images/profile_picture/user.png';

    this.resetTimer();
    this.adminservice.userActionOccured.pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe(() => {
      if (this.timerSubscription) {
        this.timerSubscription.unsubscribe();
      }
      this.resetTimer();
    });
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  resetTimer(endTime: number = this.endTime) {
    const interval = 1000;
    const duration = endTime * 60 * 30;
    this.timerSubscription = timer(0, interval).pipe(
      take(duration)
    ).subscribe(value =>
      this.render((duration - +value) * interval),
      err => { },
      () => {
        this.logOut();
      }
    );
  }

  private render(count) {
    this.seconds = this.getSeconds(count);
    this.minutes = this.getMinutes(count);
  }

  private getSeconds(ticks: number) {
    const second = ((ticks % 60000) / 1000).toFixed(0);
    return this.pad(second);
  }

  private getMinutes(ticks: number) {
    const minute = Math.floor(ticks / 60000);
    return this.pad(minute);
  }

  private pad(digit: any) {
    return digit <= 9 ? '0' + digit : digit;
  }

  logOut() {
    const currentUser = this.storage.get('adminloginSession');
    this.adminservice.logOut(currentUser)
      .pipe(first())
      .subscribe( (data: any) => {
        if (data.status) {
          this.storage.remove('adminloginSession');
          this.storage.remove('currentLanguage');
          this.router.navigate(['/admin/login']);
        }
      });
  }

}

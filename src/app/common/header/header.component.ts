import { Component, OnDestroy, OnInit, HostListener, Input, Output, EventEmitter } from '@angular/core';
import { Subject, timer, Subscription } from 'rxjs';
import { takeUntil, take, first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl, ValidatorFn } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../service/auth.service';
import { DashboardService } from '../../service/dashboard.service';
import { LocalstorageService } from '../../service/localstorage.service';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';
import { DatepickerOptions } from 'ng2-datepicker';
import * as enLocale from 'date-fns/locale/en';
import { CommonService } from '../../service/common.service';
import { TransferserviceService } from '../../service/transferservice.service';
import { DatePipe } from '@angular/common';
import { DeviceDetectorService } from 'ngx-device-detector';
import { MastersService } from '../../service/masters.service';

declare var jQuery: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnDestroy, OnInit {

  @Input() value = '';
  @Output() dateChange = new EventEmitter();

  apiSiteUrl = environment.apiSiteUrl;
  currentUserCompanyName: string;
  currentUserPicture = 'assets/images/profile_picture/user.png';
  minutes = 0;
  seconds = 0;
  endTime = 1;
  exportModal = 'none';
  userFields: any;
  userExports: any;
  serverError: any;
  exportForm: FormGroup;
  currentDate: string;
  serverError$: any;
  isError = false;
  errorMessage = '';
  isLoading = false;
  UserFullName: '';
  StreetName: '';
  PostalCode: '';
  City: '';
  PhoneNumber: '';
  Country: '';
  upgradeModal = 'none';
  accepted: 'false';
  paymentMethod: 'PDF Invoice';
  thanksModel = 'none';
  reportModel = 'none';
  isUpgradeButton = false;
  //upgradeButtonText = "Upgrade Now!";
  isExpired = false;
  URLPDF = '';
  reportUrl = '';
  startDate = "Start date";
  endDate = "End date";
  
  exportYear$:any;
  cultureForm : FormGroup;

  // startOptions: DatepickerOptions = {
  //   locale: enLocale,
  //   minYear: 2011,
  //   maxYear: 2026,
  //   minDate: new Date('2011-01-01'),
  //   maxDate: new Date('2025-12-31'),
  //   useEmptyBarTitle: false,
  //   placeholder: 'start date',
  //   barTitleIfEmpty: 'start date',
  //   addStyle: { 'border': 'none' } // Optional, value to pass to [ngStyle] on the input field
  // };

  // endOptions: DatepickerOptions = {
  //   locale: enLocale,
  //   minYear: 2011,
  //   maxYear: 2026,
  //   minDate: new Date('2011-01-01'),
  //   maxDate: new Date('2025-12-31'),
  //   useEmptyBarTitle: false,
  //   placeholder: 'end date',
  //   barTitleIfEmpty: 'end date',
  //   addStyle: { 'border': 'none' } // Optional, value to pass to [ngStyle] on the input field
  // };
  IsMobile: boolean;
  unsubscribe$: Subject<void> = new Subject();
  timerSubscription: Subscription;

  constructor(
    private auth: AuthService,
    private dashboard: DashboardService,
    private storage: LocalstorageService,
    private router: Router,
    private formBuilder: FormBuilder,
    private common: CommonService,
    private transfereService: TransferserviceService,
    private translate: TranslateService,
    private spinner: NgxSpinnerService,
    private deviceService: DeviceDetectorService,
    private deviceDetector: DeviceDetectorService,
    private master: MastersService,
  ) { 
    this.IsMobile = this.deviceDetector.isMobile();
    // if(this.translate.defaultLang === "de"){
    //   this. upgradeButtonText = "Jetzt upgraden!";
    //   this.startDate = "Startdatum";
    //   this.endDate = "Enddatum";
    // }
    // if(this.translate.defaultLang === "fr"){
    //   this. upgradeButtonText = "Jetzt upgraden!";
    //   this.startDate = "date de début";
    //   this.endDate = "jusqu'au";
    // }
  }

  ngOnInit() {
    const currentUser: any = this.storage.get('loginSession');
    this.isExpired = this.storage.get('isExpired');
    this.currentUserCompanyName = currentUser.company;
    this.currentUserPicture = (currentUser.profile_picture !== null) ? this.apiSiteUrl + 'uploads/profile_picture/' + currentUser.profile_picture : 'assets/images/profile_picture/user.png';

    this.resetTimer();
    this.auth.userActionOccured.pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe(() => {
      if (this.timerSubscription) {
        this.timerSubscription.unsubscribe();
      }
      this.resetTimer();
    });

    this.exportForm = this.formBuilder.group({
      fields: new FormArray([], this.minSelectedCheckboxes(1)),
      pdfType: ['standard', Validators.required],
      startDate: [''],
      endDate: [''],
      fieldIds: [''],
      language: [''],
    });

    this.cultureForm = this.formBuilder.group({
      year:['',[Validators.required]],
    });

    this.formControlValueChanged();
    this.getServerDate();
    this.getUserData();
    var openModel = localStorage.getItem("openUpgradeModel");
    if (openModel != undefined && openModel != null && openModel == 'true') {
      this.upgradeModal = 'block';
      localStorage.removeItem("openUpgradeModel");
    }
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
  renderDatePicker() {
    this.renderDatepicker('startDate', null);
    this.renderDatepicker('endDate', this.currentDate);
  }

  updatedateTo() {
    const from = this.exportForm.controls.startDate.value.replace(/(\d{2}).(\d{2}).(\d{4})/, "$2.$1.$3");
    this.renderDatepicker("endDate", from);
  }

  renderDatepicker(id, minDate) {
    jQuery("#" + id).datepicker("destroy");
    if (this.translate.defaultLang == 'de') {
      jQuery("#" + id).datepicker({
        firstDay: 1,
        dateFormat: 'dd.mm.yy',
        closeText: "Erledigt",
        prevText: "früher",
        nextText: "Nächster",
        currentText: "heute",
        monthNames: ["Januar", "Februar", "März", "April", "Mai", "Juni",
          "Juli", "August", "September", "Oktober", "November", "Dezember"],
        monthNamesShort: ["Jan", "Feb", "Mrz", "Apr", "Mai", "Jun",
          "Jul", "Aug", "Sept", "Okt", "Nov", "Dez"],
        dayNames: ["Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"],
        dayNamesShort: ["Son", "Mon", "Die", "Mit", "Don", "Fre", "Sam"],
        dayNamesMin: ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"],
        weekHeader: "Sem.",
        minDate: new Date(minDate),
        onSelect: (value) => {
          this.value = value;
          if (id == 'startDate') {
            this.exportForm.controls['startDate'].setValue(this.value);
            this.updatedateTo();
          } else {
            this.exportForm.controls['endDate'].setValue(this.value);
          }
        },
        beforeShow: function () {
          jQuery("#modalBody").scrollTop(0);
        }
      });
    }else if(this.translate.defaultLang == 'fr'){
      jQuery("#" + id).datepicker({
        firstDay: 1,
        dateFormat: 'dd.mm.yy',
        closeText: "Fermer",
        prevText: "Précédent",
        nextText: "Suivant",
        currentText: "Aujourd'hui",
        monthNames: ["janvier", "février", "mars", "avril", "mai", "juin",
          "juillet", "août", "septembre", "octobre", "novembre", "décembre"],
        monthNamesShort: ["janv.", "févr.", "mars", "avr.", "mai", "juin",
          "juil.", "août", "sept.", "oct.", "nov.", "déc."],
        dayNames: ["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"],
        dayNamesShort: ["dim.", "lun.", "mar.", "mer.", "jeu.", "ven.", "sam."],
        dayNamesMin: ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"],
        weekHeader: "Sem.",
        minDate: new Date(minDate),
        onSelect: (value) => {
          this.value = value;
          if (id == 'startDate') {
            this.exportForm.controls['startDate'].setValue(this.value);
            this.updatedateTo();
          } else {
            this.exportForm.controls['endDate'].setValue(this.value);
          }
        },
        beforeShow: function () {
          jQuery("#modalBody").scrollTop(0);
        }
      });
    }else{
      jQuery("#" + id).datepicker({
        firstDay: 1,
        dateFormat: 'dd.mm.yy',
        minDate: new Date(minDate),
        onSelect: (value) => {
          this.value = value;
          if (id == 'startDate') {
            this.exportForm.controls['startDate'].setValue(this.value);
            this.updatedateTo();
          } else {
            this.exportForm.controls['endDate'].setValue(this.value);
          }
        },
        beforeShow: function () {
          jQuery("#modalBody").scrollTop(0);
        }
      });
    }
    jQuery("#" + id).datepicker("refresh");
  }

  closeUpgradeModal() {
    this.upgradeModal = 'none';
  }

  addCheckboxes() {
    this.userFields.map((o, i) => {
      const control = new FormControl(i >= 0);
      (this.exportForm.controls.fields as FormArray).push(control);
    });
  }

  minSelectedCheckboxes(min = 1) {
    const validator: ValidatorFn = (formArray: FormArray) => {
      const totalSelected = formArray.controls
        .map(control => control.value)
        .reduce((prev, next) => next ? prev + next : prev, 0);
      return totalSelected >= min ? null : { required: true };
    };

    return validator;
  }

  formControlValueChanged() {
    const startDateControl = this.exportForm.get('startDate');
    const endDateControl = this.exportForm.get('endDate');
    this.exportForm.get('pdfType').valueChanges.subscribe(
      (mode: string) => {
        if (mode === 'individual') {
          startDateControl.setValidators([Validators.required, Validators.maxLength(100)]);
          endDateControl.setValidators([Validators.required]);
        } else if (mode === 'standard') {
          startDateControl.clearValidators();
          endDateControl.clearValidators();
        }
        startDateControl.updateValueAndValidity();
        endDateControl.updateValueAndValidity();
      });
  }

  getServerDate() {
    this.common.getServerDate().pipe(first()).subscribe((data: any) => {
      if (data.status) {
        this.currentDate = data.data;
        this.renderDatePicker();
        //this.exportForm.controls['startDate'].setValue(new DatePipe('en-US').transform(this.currentDate, 'MM.dd.yyyy'));
        //this.exportForm.controls['endDate'].setValue(new DatePipe('en-US').transform(this.currentDate, 'MM.dd.yyyy'));
      }
    });
  }

  modifyProfile() {
    this.upgradeModal = 'none';
    this.router.navigate(['settings']);
  }

  licenseRequest() {
    var user = this.storage.get('loginSession');
    debugger;
    var form = {
      method: 'Cash',
      comment: 'Upgraded By Self',
      userId: user.ac_userId,
      planId: 3,
      paymentOption: this.paymentMethod
    }
    this.common.upgradeUserPlan(form).pipe(first()).subscribe((data: any) => {
      this.common.sendAdminNotification({ user_id: user.token }).pipe(first()).subscribe((data: any) => {
        debugger;
        this.upgradeModal = 'none';
        this.thanksModel = 'block';
        setTimeout(() => { window.location.reload(); }, 3000);

      });

    });
  }

  closeThanksModel() {
    this.thanksModel = 'none';
  }

  getUserData() {
    this.common.getUserData().pipe(first()).subscribe((data: any) => {
      if (data.status) {
        const userData = data.data;
        if (userData.plan == "Trial") {
          this.isUpgradeButton = true;
        } else {
          var oneDay = 24 * 60 * 60 * 1000;
          let current = new Date(this.currentDate);
          let startDate = new Date(userData.subscription_start);
          let endDate = new Date(userData.subscription_end);
          var diffDays = Math.round(Math.abs((startDate.getTime() - current.getTime()) / (oneDay)));
          var trialPending = Math.round(Math.abs((current.getTime() - endDate.getTime()) / (oneDay)));
          if (trialPending <= 30) {
            this.isUpgradeButton = true;
            // if(this.translate.defaultLang === "de")
            // this.upgradeButtonText = "Bitte verl�ngern Sie Ihr Abonnement";
            // else
            // this.upgradeButtonText = "Please renew your Subscription";
          }
        }
        this.UserFullName = userData.name;
        this.StreetName = userData.street_name;
        this.PostalCode = userData.postal_code;
        this.City = userData.city;
        this.Country = userData.country;
        this.PhoneNumber = userData.phone;
      }
    });
  }

  upgrade() {
    this.accepted = null;
    this.paymentMethod = 'PDF Invoice';
    if (this.UserFullName == '' || this.StreetName == null || this.StreetName == '' || this.PostalCode == '' || this.City == '' || this.Country == '' || this.PhoneNumber == ''
      || this.PostalCode == null || this.City == null || this.Country == null || this.PhoneNumber == null) {
      localStorage.setItem("profileSetting", 'true');
      window.location.href = "/dashboard/settings";
    } else {
      this.upgradeModal = 'block';
    }
  }

  resetTimer(endTime: number = this.endTime) {
    const interval = 1000;
    const duration = endTime * 60 * 30;
    this.timerSubscription = timer(0, interval).pipe(
      take(duration)
    ).subscribe(value =>
      this.render((duration - +value) * interval),
      err => { },
      () => {
        this.logOut();
      }
    );
  }

  private render(count) {
    this.seconds = this.getSeconds(count);
    this.minutes = this.getMinutes(count);
  }

  private getSeconds(ticks: number) {
    const second = ((ticks % 60000) / 1000).toFixed(0);
    return this.pad(second);
  }

  private getMinutes(ticks: number) {
    const minute = Math.floor(ticks / 60000);
    return this.pad(minute);
  }

  private pad(digit: any) {
    return digit <= 9 ? '0' + digit : digit;
  }

  logOut() {
    const currentUser = this.storage.get('loginSession');
    this.auth.logOut(currentUser)
      .pipe(first())
      .subscribe((data: any) => {
        if (data.status) {
          const rememberMeStatus = this.storage.get('rememberMe');
          this.storage.remove('loginSession');
          this.storage.remove('currentLanguage');
          localStorage.clear();
          sessionStorage.clear();
          this.storage.removeAll();
          this.storage.set("rememberMe", rememberMeStatus);
          
          this.router.navigate(['login']);

        }
      });
  }

  get formData() { return <FormArray>this.exportForm.get('fields'); }

  openexportModal() {
    this.exportModal = 'block';
    this.isError = false;
    this.getuserAllFields();
    this.getusersAllExports();
    this.getExportYear();
  }

  getuserAllFields() {
    this.dashboard.getuserAllFields()
      .pipe(first())
      .subscribe((data: any) => {
        console.log(data);
        if (data.status) {
          this.userFields = data.data;
          this.exportForm.controls['pdfType'].setValue('standard');
          this.exportForm.controls['startDate'].setValue(this.currentDate.replace(/(\d{2}).(\d{2}).(\d{4})/, "$2.$1.$3"));
          this.exportForm.controls['endDate'].setValue(this.currentDate.replace(/(\d{2}).(\d{2}).(\d{4})/, "$2.$1.$3"));
          this.addCheckboxes();
          this.serverError = false;
        } else {
          this.serverError = true;
        }
      });
  }

  getusersAllExports() {
    this.dashboard.getusersAllExports()
      .pipe(first())
      .subscribe((data: any) => {
        if (data.status) {
          var dataExported = data.data;
          var dataPassed = [];
          dataExported.forEach(element => {
              if(element.export_type == 'Tabular'){
                this.translate.get("tabular").subscribe((result: string) => {
                  element.export_type = result;
                });
              }else if(element.export_type == 'Graphic'){
                this.translate.get("graphic").subscribe((result: string) => {
                  element.export_type = result;
                });
              }
              dataPassed.push(element);
          });
          dataExported.forEach(element => {
            if(element.sheet_data_type == 0){
              this.translate.get("crop_rotaion").subscribe((result: string) => {
                element.sheet_data_type = result;
              });
            }else if(element.sheet_data_type == 1){
              this.translate.get("culture_sheet").subscribe((result: string) => {
                element.sheet_data_type = result;
              });
            }
            dataPassed.push(element);
        });

          this.userExports = dataPassed;
          this.userExports.forEach(element => {
            ///element.created_at = new DatePipe('en-US').transform(element.created_at, 'dd.MM.yyyy');
            element.created_at =  '29.09.2019';
          });
          this.serverError = false;
        } else {
          this.serverError = true;
        }
      });
  }

  closeexportModal() {
    this.exportForm.reset();
    this.exportForm.setControl('fields', new FormArray([], this.minSelectedCheckboxes(1)));
    this.exportModal = 'none';
    this.cultureForm.reset();
  }
  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    this.exportModal = 'none';
  }

  closemodal(modalName) {
    const modal = document.getElementById(modalName + 'modalId');
    const modalContent = document.getElementById(modalName + 'ContentId');
    const that = this;
    window.onclick = function (event) {
      if (event.target === modal && event.target !== modalContent) {
        if (modalName === 'export') {
          that.closeexportModal();
        }
      }
    };
  }

  // graphicalExport() {
  //   if (this.exportForm.invalid) {
  //     return;
  //   }
  //   const currentUser: any = this.storage.get('loginSession');
  //   this.exportForm.controls['language'].setValue(currentUser.language);
  //   const selectedOrderIds = this.exportForm.value.fields.map((v, i) => v ? this.userFields[i].field_id : null).filter(v => v !== null);
  //   if (selectedOrderIds.length > 0) {
  //     this.exportForm.controls['fieldIds'].setValue(selectedOrderIds);
  //   } else {
  //     this.exportForm.controls['fieldIds'].setValue([]);
  //   }
  //   //this.spinner.show();
  //   this.isLoading = true;
  //   const formvalues = this.exportForm.value;
  //   this.dashboard.chartExportCheck(formvalues).pipe(first()).subscribe((data: any) => {
  //     if (data.status) {
  //       this.isError = false;
  //       this.errorMessage = '';
  //       this.transfereService.setinputData(formvalues);
  //       //this.spinner.hide();
  //       this.isLoading = false;
  //       this.router.navigate(['graphicalExport']);
  //     } else {
  //       this.isError = true;
  //       this.serverError$ = data.message.split('<br>').map(function (item) {
  //         return item.trim();
  //       });
  //       this.errorMessage = this.serverError$;
  //       //this.spinner.hide();
  //       this.isLoading = false;
  //     }
  //   });
  // }


  graphicalExport() {
    const startDate = this.exportForm.controls.startDate.value;
    const endDate = this.exportForm.controls.endDate.value;
    if (this.exportForm.invalid) {
      this.exportForm.controls['startDate'].setValue(startDate);
      this.exportForm.controls['endDate'].setValue(endDate);
      return;
    }

    const currentUser: any = this.storage.get('loginSession');
    this.exportForm.controls['language'].setValue(currentUser.language);

    const selectedOrderIds = this.exportForm.value.fields
      .map((v, i) => v ? this.userFields[i].field_id : null)
      .filter(v => v !== null);

    if (selectedOrderIds.length > 0) {
      this.exportForm.controls['fieldIds'].setValue(selectedOrderIds);
    } else {
      this.exportForm.controls['fieldIds'].setValue([]);
    }
    //this.spinner.show();
    this.isLoading = true;
    debugger;
    this.exportForm.controls['startDate'].setValue(startDate.replace(/(\d{2}).(\d{2}).(\d{4})/, "$3-$2-$1"));
    this.exportForm.controls['endDate'].setValue(endDate.replace(/(\d{2}).(\d{2}).(\d{4})/, "$3-$2-$1"));
    const formvalues = this.exportForm.value;
    formvalues.languageKey = currentUser.languageKey;
    this.dashboard.graphicalExport(formvalues).pipe(first()).subscribe((data: any) => {
      if (data.status) {
        debugger;
        this.isError = false;
        this.errorMessage = '';
        const file_path = data.data;
        this.reportUrl = file_path;
        //window.location.href = file_path;
        if (this.deviceService.isDesktop()) {
          window.open(file_path, "_blank");
        } else {
          this.reportModel = 'block';
        }
        this.getusersAllExports();
      } else {
        this.isError = true;
        // this.serverError$ = data.message.split('<br>').map(function (item) {
        //   return item.trim();
        // });
        this.translate.get("to_date_greater").subscribe((result: string) => {
          this.errorMessage = result;
        });
        //this.errorMessage = this.serverError$;
        this.isLoading = false;
      }
    });
    this.exportForm.controls['startDate'].setValue(startDate);
    this.exportForm.controls['endDate'].setValue(endDate);
  }

  tabularExport() {
    const startDate = this.exportForm.controls.startDate.value;
    const endDate = this.exportForm.controls.endDate.value;
    if (this.exportForm.invalid) {
      this.exportForm.controls['startDate'].setValue(startDate);
      this.exportForm.controls['endDate'].setValue(endDate);
      return;
    }

    const currentUser: any = this.storage.get('loginSession');
    this.exportForm.controls['language'].setValue(currentUser.language);

    const selectedOrderIds = this.exportForm.value.fields
      .map((v, i) => v ? this.userFields[i].field_id : null)
      .filter(v => v !== null);

    if (selectedOrderIds.length > 0) {
      this.exportForm.controls['fieldIds'].setValue(selectedOrderIds);
    } else {
      this.exportForm.controls['fieldIds'].setValue([]);
    }
    //this.spinner.show();
    this.isLoading = true;
    //debugger;
    this.exportForm.controls['startDate'].setValue(startDate.replace(/(\d{2}).(\d{2}).(\d{4})/, "$3-$2-$1"));
    this.exportForm.controls['endDate'].setValue(endDate.replace(/(\d{2}).(\d{2}).(\d{4})/, "$3-$2-$1"));
    const formvalues = this.exportForm.value;
    formvalues.languageKey = currentUser.languageKey;

    this.dashboard.tabularExport(formvalues).pipe(first()).subscribe((data: any) => {
      if (data.status) {
        //debugger;
        this.isError = false;
        this.errorMessage = '';
        const file_path = data.data;
        this.reportUrl = file_path;
        if (this.deviceService.isDesktop()) {
          window.open(file_path, "_blank");
        } else {
          this.reportModel = 'block';
        }
        //window.location.href = file_path;
        this.getusersAllExports();
      } else {
        this.isError = true;
        this.serverError$ = data.message.split('<br>').map(function (item) {
          return item.trim();
        });
        this.translate.get("to_date_greater").subscribe((result: string) => {
          this.errorMessage = result;
        });
        //this.errorMessage = this.serverError$;
        //this.spinner.hide();
        this.isLoading = false;
      }
    });
    this.exportForm.controls['startDate'].setValue(startDate);
    this.exportForm.controls['endDate'].setValue(endDate);
  }

  getExportYear(){
    this.master.getExportYear().pipe(first()).subscribe((data:any)=>{
      this.exportYear$ = data['data'];
    });
  }

  getCultureSheetExport(){
    //console.log(this.cultureForm.value);
    if(this.cultureForm.invalid){
      return;
    }
    const currentUser: any = this.storage.get('loginSession');
    const input = {'year':this.cultureForm.value.year,'language':currentUser.language};
    this.master.getCultureSheet(input).pipe(first()).subscribe((data:any)=>{
       ///console.log(data);
       if (data.status) {
        //debugger;
        this.isError = false;
        this.errorMessage = '';
        const file_path = data.data;
        this.reportUrl = file_path;
        if (this.deviceService.isDesktop()) {
          window.open(file_path, "_blank");
        } else {
          this.reportModel = 'block';
        }
       
      } else {
        this.isError = true;
        this.serverError$ = data.message.split('<br>').map(function (item) {
          return item.trim();
        });
        this.translate.get("to_date_greater").subscribe((result: string) => {
          this.errorMessage = result;
        });
        //this.errorMessage = this.serverError$;
        //this.spinner.hide();
        this.isLoading = false;
      }
    });
  }

  closeReportModel() {
    this.reportModel = 'none';
  }

}


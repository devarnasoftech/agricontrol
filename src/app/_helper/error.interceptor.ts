import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalstorageService } from '../service/localstorage.service';
import { Router } from '@angular/router';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(
        private storage: LocalstorageService,
        private router: Router
    ) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            debugger;
            if (err.status === 401) {
                this.storage.remove('loginSession');
                this.storage.remove('currentLanguage');
                this.router.navigate(['login']);
                this.storage.remove('adminloginSession');
                this.storage.remove('currentLanguage');
                location.replace("/dashboard/login");
            }
            return throwError(err);
        }))
    }
}
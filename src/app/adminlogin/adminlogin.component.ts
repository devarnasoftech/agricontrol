import { Component, OnInit } from '@angular/core';
import { AdminauthService } from '../service/adminauth.service';
import { LocalstorageService } from '../service/localstorage.service';
import { Router } from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { first } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-adminlogin',
  templateUrl: './adminlogin.component.html',
  styleUrls: ['./adminlogin.component.scss']
})
export class AdminloginComponent implements OnInit {

  loginForm: FormGroup;
  serverError$: any;
  invalidLogin = false;
  isError = false;
  isErroremail = false;
  isErrorpwd = false;
  errorMessage = '';

  constructor(
    private adminservice: AdminauthService,
    private router: Router,
    private formBuilder: FormBuilder,
    private storage: LocalstorageService,
    private spinner: NgxSpinnerService,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.storage.remove('adminloginSession');
    this.storage.remove("loginSession");
    const rememberMeStatus = this.storage.get('adminrememberMe');
    let email = '';
    let password = '';
    let rememberMe = '';
    if (rememberMeStatus && rememberMeStatus.adminrememberme) {
      email = rememberMeStatus.email;
      password = rememberMeStatus.password;
      rememberMe = rememberMeStatus.adminrememberme;
    }
    this.loginForm = this.formBuilder.group({
      email: [email, [Validators.required, Validators.maxLength(50), Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
      password: [password, [Validators.required, Validators.maxLength(25), Validators.minLength(6)]],
      adminrememberme: [rememberMe]
    });
  }

  Validationemail(){
    this.isErroremail = true;
    //this.isErrorpwd = true;
  }
  ValidationErroremail(){
    this.isErroremail = false;
   // this.isErrorpwd = false;
  }

  Validationpwd(){
    this.isErrorpwd = true;
    //this.isErrorpwd = true;
  }
  ValidationErrorpwd(){
    this.isErrorpwd = false;
   // this.isErrorpwd = false;
  }

  submitLogin() {
    if (this.loginForm.invalid) {
      return;
    }

    const formvalues = this.loginForm.value;
    this.adminservice.login(formvalues)
      .pipe(first())
      .subscribe( (data: any) => {
        if (data.status) {
          this.isError = false;
          this.storage.set('adminloginSession', data.data);
          const isRemember: boolean = this.loginForm.get('adminrememberme').value;
          if (isRemember) {
            this.storage.set('adminrememberMe', formvalues);
          } else {
            this.storage.remove('adminrememberMe');
          }
          this.router.navigate(['/admin/users']);
          this.spinner.hide();
        } else {
          this.isError = true;
          /*this.serverError$ = data.message.split('<br>').map(function(item) {
            return item.trim();
          });
          this.errorMessage = this.serverError$;
          */
          this.translate.get(data.message).subscribe((result: string) => {
            this.errorMessage = result;
          });
          this.invalidLogin = true;
          this.spinner.hide();
        }
      });
  }

}

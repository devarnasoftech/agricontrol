import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AdminauthService } from '../service/adminauth.service';
import { first } from 'rxjs/operators';

import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';


@Component({
  selector: 'app-languagefile',
  templateUrl: './languagefile.component.html',
  styleUrls: ['./languagefile.component.scss']
})
export class LanguagefileComponent implements OnInit {

  languageForm: FormGroup;
  serverError$: any;
  allLanguages$: any;
  isError = false;
  errorMessage = '';
  isSuccess = false;
  successMessage = '';
  
  arrJson: string [];
  jsonData: any;

  constructor(
    private adminservice: AdminauthService,
    private formBuilder: FormBuilder,
    private httpService: HttpClient
  ) { }
  
  ngOnInit() {
    this.languageForm = this.formBuilder.group({
      language: ['', [Validators.required]],
    });
    this.getAllLanguages();
  }
 
  getAllLanguages() {
    this.adminservice.getAllLanguages().subscribe(
      data => this.allLanguages$ = data['data']
    );
  }

  getLanguageFileData() {
    if (this.languageForm.invalid) {
      return;
    }
    //console.log(this.languageForm.controls.language.value);
    var file = this.languageForm.controls.language.value
    var path = "./assets/i18n/"+file+".json";
    this.httpService.get(path).subscribe(
      data => {
        this.arrJson = data as string [];	 // FILL THE ARRAY WITH DATA.
           
          this.jsonData = [];
          var jsonArr;
          for (var type in this.arrJson) {
              jsonArr = {};
              jsonArr.key = type;
              jsonArr.value = this.arrJson[type];
              this.jsonData.push(jsonArr);
          }
      },
      (err: HttpErrorResponse) => {
        console.log (err.message);
      }
    );
  }
  
  UpdateData(value,oldvalue,key)
  {
    ///var file = this.languageForm.controls.language.value;
    var file = this.languageForm.controls.language.value;
    var newValue = value.outerText;
    
    //console.log(file);
    //console.log((oldvalue));
    //console.log((newValue));
    ///debugger;
    
    if(oldvalue !== newValue && newValue !== oldvalue){
      //console.log('change');
      const postParam = {'language': file,'key':key,'value':newValue};
      this.adminservice.updateJsonFile(postParam).pipe(first()).subscribe( (data: any) => {
            ///console.log(data);
            if (data.status) {
              this.getLanguageFileData();
              this.isSuccess = true;
              this.successMessage = data.message;
              this.isError = false;
              
            }else{
              this.isError = true;
              this.isSuccess = false;
              this.serverError$ = data.message.split('<br>').map(function(item) {
                return item.trim();
              });
              this.errorMessage = this.serverError$;
            }
        });
        
     }
  }
  
  FadeOutLink() {
    setTimeout( () => {
          this.isSuccess = false;
        }, 2000);
  }

}




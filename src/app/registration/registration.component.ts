import { Component, OnInit } from '@angular/core';
import { AuthService } from '../service/auth.service';
import { CommonService } from '../service/common.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { LocalstorageService } from '../service/localstorage.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
})
export class RegistrationComponent implements OnInit {

  registerForm: FormGroup;
  allSubscriptions$: any;
  allSubscriptionsError = false;
  serverError$: any;
  invalidRegister = false;
  showAbtTab = true;
  showSubscriptionTab = false;
  showConfirmTab = false;
  selectedSubscription: any;
  enableSubscriptionbutton = false;
  userEmail = '';
  emailUnique = false;
  isError = false;
  isErrorname = false;
  isErrorpwd = false;
  isErroremail = false;
  isErrorconfirm = false;
  isErrorcmpy = false;
  errorMessage = '';
  isLoading = false;
  //accepted: 'false';

  constructor(
    private auth: AuthService,
    private common: CommonService,
    private formBuilder: FormBuilder,
    private storage: LocalstorageService,
    private spinner: NgxSpinnerService,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.storage.remove('loginSession');
    this.storage.remove('adminloginSession');
    this.storage.remove('currentLanguage');
    this.storage.remove('isExpired');
    this.registerForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(50)]],
      email: ['', [Validators.required, Validators.maxLength(50), Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
      password: ['', [Validators.required, Validators.maxLength(25), Validators.minLength(6)]],
      confirmPassword: ['', [Validators.required, Validators.maxLength(25), Validators.minLength(6)]],
      company: ['', [Validators.required, Validators.maxLength(100)]],
      accepted: [false, [Validators.requiredTrue]],
      subscriptionPlan: [''],
    }, { validator: this.passwordMatchValidator('password', 'confirmPassword') });

    this.getAllSubscriptionPlans();
  }

  Validationname() {
    this.isErrorname = true;
  }
  ValidationErrorname() {
    this.isErrorname = false;
  }

  Validationemail() {
    this.isErroremail = true;
  }
  ValidationErroremail() {
    this.isErroremail = false;
  }

  Validationpwd() {
    this.isErrorpwd = true;
  }
  ValidationErrorpwd() {
    this.isErrorpwd = false;
  }

  Validationconfirm() {
    this.isErrorconfirm = true;
  }
  ValidationErrorconfirm() {
    this.isErrorconfirm = false;
  }

  Validationcmpy() {
    this.isErrorcmpy = true;
  }
  ValidationErrorcmpy() {
    this.isErrorcmpy = false;
  }

  passwordMatchValidator(password: string, confirmPassword: string) {
    return (group: FormGroup): { [key: string]: any } => {
      const f = group.controls[password];
      const t = group.controls[confirmPassword];
      if (f.value && t.value && f.value.length > 5 && t.value.length > 5 && f.value !== t.value) {
        let messageError = '';
        this.translate.get("password_not_match").subscribe((result: string) => {
          messageError = result;
        });
        return {
          passwords: messageError
        };
      }
      return {};
    };
  }

  getAllSubscriptionPlans() {
    this.common.getAllSubscriptions().subscribe((data: any) => {
      if (data.status) {
        this.allSubscriptions$ = data['data'];
        this.allSubscriptionsError = false;
      } else {
        this.serverError$ = data.message.split('<br>').map(function (item) {
          return item.trim();
        });
        this.allSubscriptionsError = true;
      }
    });
  }

  SubmitAbout() {
    this.checkEmailUnique();
  }

  SubmitBack() {
    this.showAbtTab = true;
    this.showSubscriptionTab = false;
  }

  checkEmailUnique() {
    const email = this.registerForm.controls.email.value;
    this.auth.checkEmailUnique(email).pipe(first()).subscribe((data: any) => {
      if (data.status) {
        this.showAbtTab = false;
        this.showSubscriptionTab = true;
        this.emailUnique = false;
        this.userEmail = this.registerForm.controls.email.value;
      } else {
        this.showAbtTab = true;
        this.showSubscriptionTab = false;
        this.emailUnique = true;
      }
    });
  }

  selectSubscription(subscription) {
    this.selectedSubscription = subscription;
    this.enableSubscriptionbutton = true;
  }

  SubmitRegisteration() {
    if (this.registerForm.invalid) {
      return;
    }
    // if (!this.selectedSubscription) {
    //   return;
    // }
    //this.spinner.show();
    this.isLoading = true;
    const email = this.registerForm.controls.email.value;
    this.auth.checkEmailUnique(email).pipe(first()).subscribe((data: any) => {
      if (data.status) {
        this.showAbtTab = false;
        this.emailUnique = false;
        this.registerForm.value.subscriptionPlan = 0;
        const formValue = this.registerForm.value;
        if(this.translate.defaultLang == "de"){
          formValue.languageKey = 3;
        }else if(this.translate.defaultLang == "fr"){
          formValue.languageKey = 2;
        }else{
          formValue.languageKey = 1;
        }
        //formValue.languageKey = this.translate.defaultLang;
        this.auth.register(formValue).pipe(first()).subscribe((data: any) => {
          this.userEmail = this.registerForm.controls.email.value;
          //this.showConfirmTab = true;
          if (data.status) {
            this.isError = false;
            this.showSubscriptionTab = false;
            this.showConfirmTab = true;
          } else {
            this.isError = true;
            this.showAbtTab = true;
            this.showSubscriptionTab = false;
            this.invalidRegister = true;
            this.serverError$ = data.message.split('<br>').map(function (item) {
              return item.trim();
            });
            this.errorMessage = this.serverError$;
          }
          //this.spinner.hide();
          this.isLoading = false;
        });
      } else {
        this.showAbtTab = true;
        this.showSubscriptionTab = false;
        this.emailUnique = true;
        //this.spinner.hide();
        this.isLoading = false;
      }
    });
  }

  resendConfirmation() {
    //this.spinner.show();
    this.isLoading = true;
    const email = this.registerForm.controls.email.value;
    this.auth.resendconfirm(email)
      .pipe(first())
      .subscribe((data: any) => {
        if (data.status) {
          //this.spinner.hide();
          this.isLoading = false;
        } else {
          //this.spinner.hide();
          this.isLoading = false;
        }
      });
  }
}

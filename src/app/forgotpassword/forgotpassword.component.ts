import { Component, OnInit } from '@angular/core';
import { AuthService } from '../service/auth.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { first } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.scss']
})
export class ForgotpasswordComponent implements OnInit {

  forgotForm: FormGroup;
  serverError$: any;
  invalidForgot = false;
  validForgot = false;
  isError = false;
  isErrorCheck = false;
  errorMessage = '';
  isSuccess = false;
  successMessage = '';
  isLoading = false;

  constructor(
    private auth: AuthService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private translate: TranslateService
    ) { }

  ngOnInit() {
    this.forgotForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.maxLength(50), Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
    });
  }
  ValidationError(){
    this.isErrorCheck = true;
  }
  ValidationErrorCheck(){
    this.isErrorCheck = false;
  }
  submitForgot() {
    if (this.forgotForm.invalid) {
      return;
    }
    //this.spinner.show();
    this.isLoading = true;
    const formvalues = this.forgotForm.value;
    formvalues.languageKey = this.translate.defaultLang;
    this.auth.forgotPassword(formvalues)
      .pipe(first())
      .subscribe( (data: any) => {
        if (data.status) {
          this.isSuccess = true;
          this.translate.get("forgot_success_message").subscribe((result: string) => {
            this.successMessage = result;
          });
          //this.successMessage = data.message;
          this.isError = false;
          this.validForgot = true;
          this.invalidForgot = false;
          //this.spinner.hide();
          this.isLoading = false;
        } else {
          this.isError = true;
          this.isSuccess = false;
          this.translate.get("email_not_exists").subscribe((result: string) => {
            this.errorMessage = result;
          });
          //this.errorMessage = data.message;
          this.serverError$ = data.message.split('<br>').map(function(item) {
            return item.trim();
          });
          this.invalidForgot = true;
          this.validForgot = false;
          //this.spinner.hide();
          this.isLoading = false;
        }
      });
  }

}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AdminauthService } from '../service/adminauth.service';
import { first } from 'rxjs/operators';
import { LocalstorageService } from '../service/localstorage.service';
import { AlertService } from '../service/alert.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { TranslateService } from '@ngx-translate/core';

import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-adminlanguage',
  templateUrl: './adminlanguage.component.html',
  styleUrls: ['./adminlanguage.component.scss']
})
export class AdminlanguageComponent implements OnInit {

  languageForm: FormGroup;
  filterData: any;
  isError = false;
  isModalError = false;
  errorMessage = '';
  modalErrorMessage = '';
  isSuccess = false;
  successMessage = '';
  addLangModal = 'none';
  languageModal = 'none';
  serverError$: any;
  isLoading = false;
  allLanguages$: any;
  langValue$ : any;
  
  searchForm: FormGroup;
  error = false;
  errorMsg = '';
  success = false;
  successMsg = '';
  arrJson: string [];
  jsonData: any;
  key_name:any;

  updateButton = false;

  updateArr:string[];

  constructor(
    private adminservice: AdminauthService,
    private formBuilder: FormBuilder,
    private storage: LocalstorageService,
    private httpService: HttpClient,
    private spinner: NgxSpinnerService,
    private alertService: AlertService,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.languageForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(50)]],
      symbol: ['', [Validators.required, Validators.maxLength(3),Validators.minLength(2)]],
      langId: ['', Validators.required],
    });

    this.searchForm = this.formBuilder.group({
      language: ['', [Validators.required]],
    });
    
    this.getAllLanguages();
    this.updateArr = [];
  }
  
  getAllLanguages() {
    this.adminservice.getAllLanguageKeyValue().subscribe(
      (data: any) => {
        this.allLanguages$ = data['data'];
        this.filterData = data['data'];
        this.langValue$ = data['keyvalue'];

        this.arrJson = this.langValue$ as string [];	 // FILL THE ARRAY WITH DATA.
                  
        this.jsonData = [];
        var jsonArr;
        for (var type in this.arrJson) {
            jsonArr = {};
            jsonArr.key = type;
            jsonArr.value = this.arrJson[type];
            this.jsonData.push(jsonArr);
        }
        ///console.log(this.langValue$);
      } 
    );
  }
  
  editLanguage(lang_id) {
    this.languageModal = 'block';
    const currentUser: any = this.storage.get('adminloginSession');
    this.languageForm.reset();
    const Input = { 'lang_id': lang_id };
    this.adminservice.getLanguageById(Input).pipe(first()).subscribe((data: any) => {
      if (data.status) {
        const langData = data.data;
        this.languageForm.setValue({
          symbol: langData.symbol,
          name: langData.name,
          langId: langData.ac_languageId,
        });
      }
    });
  }

  openaddLanguage() {
    this.addLangModal = 'block';
    this.languageForm.reset();
    this.languageForm.setValue({
      name: "",
      symbol: "",
      langId: 0,
    });
  }

  updateLanguage() {
    if (this.languageForm.invalid) {
      return;
    }
    const Input = { 'name': this.languageForm.value.name,'langId' : this.languageForm.value.langId };
    this.adminservice.updateLanguageData(Input).pipe(first()).subscribe((data: any) => {
      if (data.status) {
        this.isSuccess = true;
        this.successMessage = data.message;
        this.isModalError = false;
        this.modalErrorMessage = '';
        this.closeLanguageModal();
        this.getAllLanguages();
      } else {
        this.isModalError = true;
        this.serverError$ = data.message.split('<br>').map(function (item) {
          return item.trim();
        });
        this.modalErrorMessage = this.serverError$;
      }
    });
  }

  addLanguage() {
    if (this.languageForm.invalid) {
      return;
    }
    this.adminservice.addLanguageData(this.languageForm.value).pipe(first()).subscribe((data: any) => {
      //console.log(data);
      if (data.status) {
        this.isSuccess = true;
        this.successMessage = data.message;
        this.isModalError = false;
        this.modalErrorMessage = '';
        this.closeaddLangModal();
        this.getAllLanguages();
      } else {
        this.isModalError = true;
        this.serverError$ = data.message.split('<br>').map(function (item) {
          return item.trim();
        });
        this.modalErrorMessage = this.serverError$;
      }
    });
  }

  closeLanguageModal() {
    this.languageForm.reset();
    this.isError = false;
    this.isModalError = false;
    this.languageModal = 'none';
  }

  closeaddLangModal() {
    this.languageForm.reset();
    this.isError = false;
    this.isModalError = false;
    this.addLangModal = 'none';
  }

  closemodal(modalName) {
    const modal = document.getElementById(modalName + 'modalId');
    const modalContent = document.getElementById(modalName + 'ContentId');
    const that = this;
    window.onclick = function (event) {
      if (event.target === modal && event.target !== modalContent) {
        if (modalName === 'language') {
          that.languageForm.reset();
          that.closeLanguageModal();
          that.closeaddLangModal();
        }
        modal.style.display = 'none';
      }
    };
  }

  /*search(term: string) {
    if (term === '') {
      this.filterData = this.allCrops$;
    } else {
      this.filterData = this.allCrops$.filter(x =>
        x.crop_name.trim().toLowerCase().includes(term.trim().toLowerCase())
      );
      if (this.filterData.length === 0) {
        this.filterData = this.allCrops$.filter(x =>
          x.crop_family_name.trim().toLowerCase().includes(term.trim().toLowerCase())
        );
      }
    }
  }*/

  getLanguageFileData() {
    if (this.searchForm.invalid) {
      return;
    }
    //console.log(this.searchForm.controls.language.value);
    var file = this.searchForm.controls.language.value
    var path = "./assets/i18n/"+file+".json";
    this.httpService.get(path).subscribe(
      data => {
        this.arrJson = data as string [];	 // FILL THE ARRAY WITH DATA.
           
          this.jsonData = [];
          var jsonArr;
          for (var type in this.arrJson) {
              jsonArr = {};
              jsonArr.key = type;
              jsonArr.value = this.arrJson[type];
              this.jsonData.push(jsonArr);
          }
          
      },
      (err: HttpErrorResponse) => {
        console.log (err.message);
      }
    );
  }

  UpdateDataArray(value,oldvalue,keyname,symbol)
  {
    //var newValue = value.outerText;
    //console.log(value);
    var newValue = value.outerText;
    if(oldvalue !== newValue){
      var newArr;
      /*newArr = {};
      newArr.key = key;
      newArr.value = newValue;
      */
      //var newKey = '"'+key+'"';
      newArr = {};
      newArr[keyname] = newValue;
      newArr['symbol'] = symbol;
      
      this.updateButton = true;
      this.updateArr.push(newArr);
      ///console.log(this.updateArr);
    }
  }
  
  /*UpdateData(value,oldvalue,key)
  {
    ///var file = this.languageForm.controls.language.value;
    var file = this.searchForm.controls.language.value;
    var newValue = value.outerText;
    
    var newArr;
    newArr = [];
    newArr.key = key;
    newArr.value = newValue;

    this.updateArr.push(newArr);
    
    
    //console.log(file);
    //console.log((oldvalue));
    //console.log((newValue));
    ///debugger;
    
    if(oldvalue !== newValue && newValue !== oldvalue){
      //console.log('change');
      const postParam = {'language': file,'key':key,'value':newValue};
      this.adminservice.updateJsonFile(postParam).pipe(first()).subscribe( (data: any) => {
            ///console.log(data);
            if (data.status) {
              this.getLanguageFileData();
              this.success = true;
              this.successMsg = data.message;
              this.error = false;
              
            }else{
              this.error = true;
              this.success = false;
              this.serverError$ = data.message.split('<br>').map(function(item) {
                return item.trim();
              });
              this.errorMsg = this.serverError$;
            }
        });
     }
     
  }*/

  updateLanguageFileData(){
    //console.log( this.searchForm.value);
    //console.log(this.updateArr);
    //var file = this.searchForm.controls.language.value;
    var file = '';
    if(this.updateArr){
      const postParam = {'language': file,'update_array':this.updateArr};
      this.adminservice.updateJsonFile(postParam).pipe(first()).subscribe( (data: any) => {
            //console.log(data);
            if (data.status) {
              this.getAllLanguages();
              this.success = true;
              this.successMsg = data.message;
              this.error = false;
              
            }else{
              this.error = true;
              this.success = false;
              this.serverError$ = data.message.split('<br>').map(function(item) {
                return item.trim();
              });
              this.errorMsg = this.serverError$;
            }
        });
    }
  }

  languageActivation(ac_languageId,name) {
    //console.log(name);
    const that = this;
    const confirmMsg = 'Möchtest du wirklich veröffentlichen zum ' + name;
    that.alertService.confirmThis('Confirmation', confirmMsg, function () {
      that.spinner.show();
      const Input = {'status': 1, 'ac_languageId': ac_languageId };
      that.adminservice.publishLanguage(Input).pipe(first()).subscribe((data: any) => {
        if (data.status) {
          that.isSuccess = true;
          that.isError = false;
          //that.successMessage = data.message;
          that.errorMessage = '';
          that.spinner.hide();
          that.getAllLanguages();
        } else {
          that.isSuccess = false;
          that.isError = true;
          that.successMessage = '';
          //that.errorMessage = data.message;
          that.spinner.hide();
        }
        ///console.log(data.message);
        if(data.message && data.message != ''){
          that.translate.get(data.message).subscribe((result: string) => {
            if(that.isSuccess == true){
              that.successMessage = result;
            }else{
              that.errorMessage = result;
            }
          }); 
        }

      });
    }, function () {
      console.log('No');
    });
    
  }

  FadeOutLink() {
    setTimeout( () => {
          this.isSuccess = false;
          this.isError = false;
          this.isModalError = false;
          this.error = false;
          this.success = false;
        }, 2000);
  }

}

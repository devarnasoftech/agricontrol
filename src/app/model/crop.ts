export class Crop {
    cropId: number;
    name: string;
    family: string;
    color: string;
    language: string;
}

import { DashboarCulture } from "./dashboard_cultures";
import { fieldSizeData } from "./field_size_data";

export class DashboardFieldData {
    
    field_id : number;
    field_name : string;
    field_size : number;
    echo : number;
    echo_size : number;
    magnesium : number;
    max_culture_count : number;
    nitrogen : number;
    notes : string;
    ph_value : number;
    phosphorus : number;
    potassium_oxide : number;
    soil_sample_date : Date;
    zipcode : number;
    city : string;
    archieve : number;
    address : string;
    cultures : DashboarCulture[];
    field_size_data : fieldSizeData[];

}
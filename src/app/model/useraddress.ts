export class UserAddress {
    streetName: string;
    city: string;
    country: string;
    postalCode: number;
    language: number;
    email: string;
    phone: string;
}

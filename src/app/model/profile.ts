export class Profile {
    userId: string;
    name: string;
    email: string;
    company: string;
    language: number;
    phone: string;
    streetName: string;
    postalCode: number;
    city: string;
    country: string;
}

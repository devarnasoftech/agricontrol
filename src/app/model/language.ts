export class Language {
    langId: number;
    name: string;
    symbol: string;
}

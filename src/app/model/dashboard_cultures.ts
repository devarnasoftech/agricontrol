
export class DashboarCulture {
    
    culture_id : number;
    culture_notes : string;
    culture_size : number;
    duration : number;
    color : string;
    crop_family_id : number;
    crop_family_name : string;
    crop_id : number;
    crop_name : string;
    end_date : Date;
    harvest_volume : number;
    is_filter : number;
    is_swapped : number;
    plantVariety : string;
    rule_breaked : number;
    seed_volume : number;
    start_date : Date;
    swapped_with : string;
    before_culture_crop_name : [];
    before_culture_end_date : [];
    before_culture_size : [];
    before_culture_start_date : [];
} 
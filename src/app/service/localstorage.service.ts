import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LocalstorageService {

  constructor() { }

  set(key: string, value: any) {
    if (key && value) {
      const stringifyValue: string = JSON.stringify(value);
      localStorage.setItem(key, stringifyValue);

      return true;
    }
    return false;
  }

  get(key: string) {
    if (key) {
      const value: string = localStorage.getItem(key);
      if (value) {
        const jsonvalue: any = JSON.parse(value);

        return jsonvalue;
      }
    }
    return false;
  }

  remove(key: string) {
    if (key) {
      localStorage.removeItem(key);
    }
    return false;
  }

  removeAll() {
    localStorage.clear();
    return true;
  }

  getAuthToken() {
    const loginSession = this.get('loginSession');
    if (loginSession) {
      const authToken = loginSession.token;
      if (authToken !== '') {
        var now = new Date();
        var end = new Date(loginSession.subscription_end);
        end.setDate(end.getDate() + 7);
        if (now > end) {
          this.remove("loginSession");
          return false;
        }
        return authToken;
      }
    }
    return false;
  }

  getAdminAuthToken() {
    const loginSession = this.get('adminloginSession');
    if (loginSession) {
      const authToken = loginSession.token;
      if (authToken !== '') {
        return authToken;
      }
    }
    return false;
  }

  getAuthHeader() {
    ///debugger;
    const currentUserToken = this.getAuthToken();
    let httpOptions: any;
    if (currentUserToken) {
      httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'X-Auth-Token': currentUserToken,
          'Cache-Control': 'no-cache,no-store',
          'Pragma': 'no-cache'
        })
      };
    } else {
      httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache,no-store',
          'Pragma': 'no-cache'
        })
      };
    }
    //debugger;
    return httpOptions;
  }

  getImageAuthHeader() {
    const currentUserToken = this.getAuthToken();
    let httpOptions: any;
    if (currentUserToken) {
      httpOptions = {
        headers: new HttpHeaders({
          'X-Auth-Token': currentUserToken
        })
      };
    }
    return httpOptions;
  }

  getAdminImageAuthHeader() {
    const currentUserToken = this.getAdminAuthToken();
    let httpOptions: any;
    if (currentUserToken) {
      httpOptions = {
        headers: new HttpHeaders({
          'X-Auth-Token': currentUserToken
        })
      };
    }
    return httpOptions;
  }

  getAdminAuthHeader() {
    const currentUserToken = this.getAdminAuthToken();
    let httpOptions: any;
    if (currentUserToken) {
      httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'X-Auth-Token': currentUserToken
        })
      };
    } else {
      httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        })
      };
    }
    return httpOptions;
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LocalstorageService } from './localstorage.service';

import { Field } from '../model/field';
import { Culture } from '../model/culture';
import { environment } from '../../environments/environment';
import { Profile } from '../model/profile';
import { Observable } from 'rxjs';
import { DashboardData } from '../model/dashboard_data';
import { ExportFieldData } from '../model/export_field_data';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  apiBaseUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient, private storage: LocalstorageService) { }

  addField(field: Field) {
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'dashboard/addField', field ,authHeader);
  }

  addFieldCulture(culture: Culture) {
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'dashboard/addFieldCulture', culture ,authHeader);
  }

  deleteCulture(input) {
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'dashboard/deleteCulture', input ,authHeader);
  }

  addCultureWithCropRotationWarning(culture: Culture) {
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'dashboard/addCultureWithCropRotationWarning', culture ,authHeader);
  }

  /*getDashboardChartDataTest(range): Observable<DashboardData> {
    return this.http.get<DashboardData>(this.apiBaseUrl + 'dashboard/getDashboardChartData/?min='+range.min+',max='+range.max+',language='+range.language);
  }*/

  /*getDashboardChartDataTest(range): Observable<ExportFieldData[]> {
    return this.http.get<ExportFieldData[]>(this.apiBaseUrl + 'dashboard/getusersAllFields').pipe();
  }*/

  getDashboardChartData(range){
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'dashboard/getDashboardChartData/',range,authHeader);
  }

  getuserAllFields() {
    const authHeader = this.storage.getAuthHeader();
    return this.http.get(this.apiBaseUrl + 'dashboard/getusersAllFields' ,authHeader);
  }

  getusersAllExports() {
    const authHeader = this.storage.getAuthHeader();
    return this.http.get(this.apiBaseUrl + 'dashboard/getusersAllExports' ,authHeader);
  }

  getDashboardTopBoxesData(Input) {
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'dashboard/getDashboardTopBoxesData', Input ,authHeader);
  }

  getsizePerCategoryByYear(Input) {
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'dashboard/getsizePerCategoryByYear', Input ,authHeader);
  }

  getSingleFieldDataExportByReportingYear(range) {
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'dashboard/getSingleFieldDataExportByReportingYear', range ,authHeader);
  }

  getUserMostPlantedCrops(range) {
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'dashboard/getUserMostPlantedCrops', range ,authHeader);
  }

  getUserPlannedFieldCrops(fieldIdArr) {
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'dashboard/getUserPlannedFieldCrops', fieldIdArr ,authHeader);
  }

  getFieldCultureMinMaxDate(fieldId) {
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'dashboard/getFieldCultureMinMaxDate', fieldId ,authHeader);
  }

  getSingleFieldChartData(range) {
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'dashboard/getSingleFieldChartData', range ,authHeader);
  }

  archiveField(archieveInput) {
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'dashboard/archiveField', archieveInput ,authHeader);
  }

  getFieldRemainingSpace(InputData) {
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'dashboard/getFieldRemainingSpace', InputData ,authHeader);
  }

  tabularExport(InputData) {
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'dashboard/tabularExport', InputData ,authHeader);
  }

  graphicalExport(InputData) {
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'dashboard/graphicalExport', InputData ,authHeader);
  }

  chartExportCheck(InputData) {
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'dashboard/chartExportCheck', InputData ,authHeader);
  }

  getchartExportData(InputData) {
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'dashboard/getchartExportData', InputData ,authHeader);
  }

  deleteField(input) {
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'dashboard/deleteField', input ,authHeader);
  }

}

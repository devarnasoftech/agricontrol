import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { LocalstorageService } from './localstorage.service';

import { User } from '../model/user';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  apiBaseUrl = environment.apiBaseUrl;

  _userActionOccured: Subject<void> = new Subject();
  get userActionOccured(): Observable<void> { return this._userActionOccured.asObservable(); }

  constructor(private http: HttpClient, private storage: LocalstorageService) { }

  login(user: User) {
    return this.http.post(this.apiBaseUrl + 'auth/login', user);
  }

  forgotPassword(user: User) {
    return this.http.post(this.apiBaseUrl + 'auth/forgotPassword', user);
  }

  setPassword(user: User) {
    return this.http.post(this.apiBaseUrl + 'auth/setPassword', user);
  }

  checkEmailToken(token: any) {
    return this.http.post(this.apiBaseUrl + 'auth/checkEmailToken', token);
  }

  register(user: User) {
    return this.http.post(this.apiBaseUrl + 'auth/register', user);
  }

  checkEmailUnique(user: User) {
    return this.http.post(this.apiBaseUrl + 'auth/checkEmailUnique', {'email': user});
  }

  resendconfirm(user: User) {
    return this.http.post(this.apiBaseUrl + 'auth/resendConfirm', {'email': user});
  }

  confirmEmail(user: User) {
    return this.http.post(this.apiBaseUrl + 'auth/confirmEmailId', {'emailToken': user});
  }

  isLoggednIn() {
    return this.storage.getAuthToken();
  }

  isAdminLogIn(){
    var admin =  this.storage.get('adminloginSession');
    if(admin != null && admin != false){
      return true;
    }
  }

  notifyUserAction() {
    this._userActionOccured.next();
  }

  logOut(user: User) {
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'auth/logout', user, authHeader);
  }

  restoreAccount(Input){
    return this.http.post(this.apiBaseUrl + 'auth/restoreUserAccount', Input);
  }

}

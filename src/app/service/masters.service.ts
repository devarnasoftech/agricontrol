import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LocalstorageService } from './localstorage.service';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MastersService {

  apiBaseUrl = environment.apiBaseUrl;

  constructor(
    private http: HttpClient,
    private storage: LocalstorageService
  ) { }

  addMachine(data) {
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'master/actionMachine', data, authHeader); 
  }

  getAllMachine() {
    const authHeader = this.storage.getAuthHeader();
    return this.http.get(this.apiBaseUrl + 'master/getAllMachine', authHeader); 
  }

  deleteMachine(input){
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'master/deleteMachine', input, authHeader); 
  }

  actionSoil(data){
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'master/actionSoil',data,authHeader);
  }

  getAllSoil(){
    const authHeader = this.storage.getAuthHeader();
    return this.http.get(this.apiBaseUrl + 'master/getAllSoil',authHeader);
  }

  deleteSoil(input){
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'master/deleteSoil',input , authHeader);
  }

  actionPests(data){
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'master/actionPests',data, authHeader);
  }

  getAllPests(){
    const authHeader = this.storage.getAuthHeader();
    return this.http.get(this.apiBaseUrl + 'master/getAllPests' ,authHeader);
  }

  deletePests(input){
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'master/deletePests' ,input ,authHeader);
  }
   
  actionFertilizer(data){
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'master/actionFertilizer', data, authHeader);
  } 

  getAllFertilizer(){
    const authHeader = this.storage.getAuthHeader();
    return this.http.get(this.apiBaseUrl + 'master/getAllFertilizer' ,authHeader);
  }

  deleteFertilizer(input){
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'master/deleteFertilizer' , input,authHeader);
  }

  getAllPesticideType(){
    const authHeader = this.storage.getAuthHeader();
    return this.http.get(this.apiBaseUrl + 'master/getAllPesticideType',authHeader);
  }

  actionPestiscide(data){
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'master/actionPestiscide', data, authHeader);
  }

  getAllPesticide(){
    const authHeader = this.storage.getAuthHeader();
    return this.http.get(this.apiBaseUrl + 'master/getAllPesticide', authHeader);
  }

  deletePestiscide(input){
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'master/deletePestiscide', input,authHeader);
  }

  plantActivity(data){
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'master/plantActivity', data, authHeader);
  }

  getAllPlantActivity(input){
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'master/getAllPlantActivity' ,input, authHeader);
  }

  deletePlantActivty(input){
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'master/deletePlantActivty' ,input, authHeader);
  }

  soilActivity(data){
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'master/soilActivity', data, authHeader);
  }

  getAllSoilActivity(input){
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'master/getAllSoilActivity' ,input, authHeader);
  }

  deleteSoilActivty(input){
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'master/deleteSoilActivty' ,input, authHeader);
  }

  fertilizerCultureDetail(input){
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'master/fertilizerCultureDetail' ,input, authHeader);
  }

  getFertilizerDetail(input){
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'master/getFertilizerDetail' ,input, authHeader);
  }

  fertilizerActivity(data){
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'master/fertilizerActivity' ,data, authHeader);
  }

  getAllFertilizerActivity(input){
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'master/getAllFertilizerActivity' ,input, authHeader);
  }

  getFertilizerActivityDetail(input){
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'master/getFertilizerActivityDetail' ,input, authHeader);
  }

  deleteFertilizerActivty(input){
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'master/deleteFertilizerActivty' ,input, authHeader);
  }

  getExportYear(){
    const authHeader = this.storage.getAuthHeader();
    return this.http.get(this.apiBaseUrl + 'master/getExportYear' , authHeader);
  }

  getCultureSheet(input){
    const authHeader = this.storage.getAuthHeader();
    return this.http.post(this.apiBaseUrl + 'master/getCultureSheet' ,input, authHeader);
  }

}

import { TestBed } from '@angular/core/testing';

import { DeletealertService } from './deletealert.service';

describe('DeletealertService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DeletealertService = TestBed.get(DeletealertService);
    expect(service).toBeTruthy();
  });
});

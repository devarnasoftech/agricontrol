import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DataTableModule } from 'angular-6-datatable';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AuthService } from './service/auth.service';
import { AdminauthService } from './service/adminauth.service';
import { LocalstorageService } from './service/localstorage.service';

import { AuthGuard } from './guards/auth.guard';
import { AdminauthGuard } from './guards/adminauth.guard';
import { Ng5SliderModule } from 'ng5-slider';
import { NgxSpinnerModule } from 'ngx-spinner';
import { IconsModule } from './icons/icons.module';

import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ConfirmEmailComponent } from './confirmemail/confirmemail.component';
import { HeaderComponent } from './common/header/header.component';
import { RegisterFieldsComponent } from './registerfields/registerfields.component';
import { NgDatepickerModule } from 'ng2-datepicker';
import { CropmasterComponent } from './cropmaster/cropmaster.component';
import { FaqComponent } from './faq/faq.component';
import { SettingComponent } from './setting/setting.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AlertComponent } from './alert/alert.component';
import { TitleCasePipe } from '@angular/common';
import { DatePipe } from '@angular/common';
import { FooterComponent } from './common/footer/footer.component';
import { LimitTo } from './custompipe/limit-to.pipe';
import { GraphicalExportComponent } from './graphical-export/graphical-export.component';
import { ColorPickerModule } from 'ngx-color-picker';
import { AdminloginComponent } from './adminlogin/adminlogin.component';
import { AdminheaderComponent } from './common/adminheader/adminheader.component';
import { AdminusersComponent } from './adminusers/adminusers.component';
import { AdmincropsComponent } from './admincrops/admincrops.component';
import { AdminprofileComponent } from './adminprofile/adminprofile.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { MainContainerComponent } from './main-container/main-container.component';
import { MastersComponent } from './masters/masters.component';
import { LanguagefileComponent } from './languagefile/languagefile.component';
import { AdminlanguageComponent } from './adminlanguage/adminlanguage.component';
import {A2Edatetimepicker} from 'ng2-eonasdan-datetimepicker';
import { DeletealertComponent } from './deletealert/deletealert.component';

// search module
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { ErrorInterceptor } from './_helper/error.interceptor';
import { HighlightSearch } from './service/highlightsearch';
import { DeviceDetectorModule } from 'ngx-device-detector';
import { RedirectGuard } from './_helper/redirectGuard';
import { AdmindashboardComponent } from './admindashboard/admindashboard.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    RegistrationComponent,
    LoginComponent,
    DashboardComponent,
    ConfirmEmailComponent,
    HeaderComponent,
    RegisterFieldsComponent,
    CropmasterComponent,
    FaqComponent,
    SettingComponent,
    ForgotpasswordComponent,
    ResetpasswordComponent,
    AlertComponent,
    FooterComponent,
    LimitTo,
    GraphicalExportComponent,
    AdminloginComponent,
    AdminheaderComponent,
    AdmindashboardComponent,
    AdminusersComponent,
    AdmincropsComponent,
    AdminprofileComponent,
    PageNotFoundComponent,
    MainContainerComponent,
    HighlightSearch,
    LanguagefileComponent,
    AdminlanguageComponent,
    MastersComponent,
    DeletealertComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    Ng5SliderModule,
    NgxSpinnerModule,
    NgDatepickerModule,
    IconsModule,
    ColorPickerModule,
    DataTableModule,
    A2Edatetimepicker,
    Ng2SearchPipeModule,
    DeviceDetectorModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    ServiceWorkerModule.register('./ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [ AuthService, LocalstorageService, AuthGuard, AdminauthGuard, AdminauthService, TitleCasePipe, DatePipe,
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }, RedirectGuard],
    
  bootstrap: [AppComponent]
})
export class AppModule { }

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

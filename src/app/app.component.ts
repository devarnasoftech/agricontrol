import { Component, HostListener, DoCheck, OnDestroy, OnInit, OnChanges } from '@angular/core';
import { AuthService } from './service/auth.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { LocalstorageService } from './../app/service/localstorage.service';
import { CommonService } from './service/common.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements DoCheck {
  title = 'Agricontrol';
  isHeader = false;
  isAdmin = false;
  selectedLanguage = "de";
  currentURL = "";
  allLanguages$: any;
  langArr : any;

  constructor(
    private auth: AuthService,
    private translate: TranslateService,
    private localstorage: LocalstorageService,
    private router: Router,
    private common: CommonService
  ) {
    let session = this.localstorage.get("loginSession");
    if (session && session.languageKey) {
      translate.setDefaultLang(session.languageKey);
    } else {
      this.selectedLanguage = translate.getDefaultLang();
      if (this.selectedLanguage === undefined) {
        if (this.localstorage.get("defaultLanguage") === false) {
          this.localstorage.set("defaultLanguage", "en");
          this.selectedLanguage = "en";
        } else {
          this.selectedLanguage = this.localstorage.get("defaultLanguage");
        }
        translate.setDefaultLang(this.selectedLanguage);
      }
    }
    this.languageList();

  }

  languageList() {
    this.common.allLanguages().subscribe((data: any) => {
      
      //this.allLanguages$ = data['data'];
      this.langArr = [];
      var jsonArr;
      for(let x of data['data']) {
        if (x.is_publish == 1) {
          //console.log(x);
          jsonArr = {};
          jsonArr['ac_languageId'] = x.ac_languageId;
          jsonArr['name'] = x.name;
          jsonArr['symbol'] = x.symbol;
          jsonArr['is_publish'] = x.is_publish;
          this.langArr.push(jsonArr);
        }
      }
      ///console.log(this.langArr);
      this.allLanguages$ = this.langArr;
    });
  }

  changedLanguage() {
    this.translate.setDefaultLang(this.selectedLanguage);
    this.localstorage.set("defaultLanguage", this.selectedLanguage);
  }

  @HostListener('document:keyup', ['$event'])
  @HostListener('document:click', ['$event'])
  @HostListener('document:wheel', ['$event'])
  resetTimer() {
    this.auth.notifyUserAction();
  }
  ngDoCheck(): void {
    if (this.auth.isAdminLogIn()) {
      this.isAdmin = true;
    }
    if (this.auth.isLoggednIn()) {
      this.isHeader = true;
    } else {
      this.isHeader = false;
    }
  }

  ngAfterViewInit(): void {
    this.router.url;
  }
}

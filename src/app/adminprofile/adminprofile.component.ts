import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from '../service/common.service';
import { SettingService } from '../service/setting.service';
import { AdminauthService } from '../service/adminauth.service';
import { first } from 'rxjs/operators';
import { LocalstorageService } from '../service/localstorage.service';
import { AlertService } from '../service/alert.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-adminprofile',
  templateUrl: './adminprofile.component.html',
  styleUrls: ['./adminprofile.component.scss']
})
export class AdminprofileComponent implements OnInit {

  urls = new Array<string>();
  //To show image in preview
  detectFiles(event) {
    this.urls = [];
    let files = event.target.files;
    if (files) {
      for (let file of files) {
        let reader = new FileReader();
        reader.onload = (e: any) => {
          this.urls.push(e.target.result);
        }
        reader.readAsDataURL(file);
      }
    }
  }

  profileForm: FormGroup;
  changePasswordForm: FormGroup;
  serverError$: any;
  allLanguages$: any;
  currentUser$: any;
  isError = false;
  errorMessage = '';
  isSuccess = false;
  successMessage = '';
  isPasswordError = false;
  PassworderrorMessage = '';
  isPasswordSuccess = false;
  PasswordsuccessMessage = '';
  fileInput: File = null;
  filevalidationError = false;
  filevalidationErrorMsg = '';
  isSubmit = false;
  langArr : any;

  constructor(
    private common: CommonService,
    private adminservice: AdminauthService,
    private setting: SettingService,
    private formBuilder: FormBuilder,
    private storage: LocalstorageService,
    private router: Router,
    private alertService: AlertService,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.profileForm = this.formBuilder.group({
      image: [''],
      name: ['', [Validators.required, Validators.maxLength(50)]],
      oldemail: [''],
      email: ['', [Validators.required, Validators.maxLength(50), Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
      company: ['', [Validators.required, Validators.maxLength(100)]],
      phone: ['', [Validators.required, Validators.maxLength(15)]],
      language: ['', [Validators.required]],
      streetName: ['', [Validators.required, Validators.maxLength(150)]],
      postalCode: ['', [Validators.required, Validators.maxLength(10), Validators.pattern('[0-9 ]*')]],
      city: ['', [Validators.required, Validators.maxLength(100), Validators.pattern('[A-zÀ-ÿ ]+')]],
      country: ['', [Validators.required, Validators.maxLength(100), Validators.pattern('[A-zÀ-ÿ ]+')]],
    });

    this.changePasswordForm = this.formBuilder.group({
      oldPassword: ['', [Validators.required, Validators.maxLength(25), Validators.minLength(6)]],
      password: ['', [Validators.required, Validators.maxLength(25), Validators.minLength(6)]],
      confirmPassword: ['', [Validators.required, Validators.maxLength(25), Validators.minLength(6)]],
    }, { validator: this.passwordMatchValidator('password', 'confirmPassword') });

    this.getUserData();
    this.getAllLanguages();

    this.currentUser$ = this.storage.get('adminloginSession');
    ///console.log(this.currentUser$);
  }

  passwordMatchValidator(password: string, confirmPassword: string) {
    return (group: FormGroup): { [key: string]: any } => {
      const f = group.controls[password];
      const t = group.controls[confirmPassword];
      if (f.value && t.value && f.value.length > 5 && t.value.length > 5 && f.value !== t.value) {
        return {
          passwords: 'Passwort und Passwort bestätigen stimmen nicht überein.'
        };
      }
      return {};
    };
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  checkPhone(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    const inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode !== 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  onFileChanged(event) {
    this.fileInput = <File>event.target.files[0];
    const fileExtension = this.fileInput.name.split('.').pop();
    const allowedExtensions = /(\jpg|\jpeg|\png|\gif)$/i;
    if (!allowedExtensions.exec(fileExtension.toLowerCase())) {
      this.filevalidationError = true;
      this.filevalidationErrorMsg = 'Bitte laden Sie nur Dateien mit den Endungen .jpeg / .jpg / .png / .gif hoch.';
      this.profileForm.get('image').reset();
      return false;
    } else {
      const fileSize = this.fileInput.size / 1024 / 1024; // in MB
      if (fileSize > 3) {
        this.filevalidationError = true;
        this.filevalidationErrorMsg = 'Dateigröße überschreitet 3 MB.';
        this.profileForm.get('image').reset();
        return false;
      } else {
        window.URL = window.URL || (window as any).webkitURL;
        const reader = new FileReader();
        const img = new Image();
        img.src = window.URL.createObjectURL(this.fileInput);
        reader.readAsDataURL(this.fileInput);
        reader.onload = () => {
          const width = img.naturalWidth;
          const height = img.naturalHeight;
          window.URL.revokeObjectURL(img.src);
          if (width > 2000 && height > 2000) {
            this.filevalidationError = true;
            this.filevalidationErrorMsg = 'Das Foto sollte eine Größe von weniger als 2000 x 2000 haben.';
            this.profileForm.get('image').reset();
            return false;
          } else {
            this.filevalidationError = false;
            this.filevalidationErrorMsg = '';
            return true;
          }
        };
      }
    }
  }

  getUserData() {

    this.adminservice.getUserData()
      .pipe(first())
      .subscribe((data: any) => {
        if (data.status) {
          const userData = data.data;
          this.profileForm.setValue({
            image: '',
            name: userData.name,
            email: userData.email,
            company: userData.company,
            phone: userData.phone,
            language: userData.language,
            streetName: userData.street_name,
            postalCode: userData.postal_code,
            city: userData.city,
            country: userData.country,
            oldemail: userData.email
          });
        }
      });
  }

  getAllLanguages() {
    this.adminservice.getAllLanguages().subscribe((data: any) => {

      this.langArr = [];
      var jsonArr;
      for(let x of data['data']) {
        if (x.is_publish == 1) {
          //console.log(x);
          jsonArr = {};
          jsonArr['ac_languageId'] = x.ac_languageId;
          jsonArr['name'] = x.name;
          jsonArr['symbol'] = x.symbol;
          jsonArr['is_publish'] = x.is_publish;
          this.langArr.push(jsonArr);
        }
      }
      ///console.log(this.langArr);
      this.allLanguages$ = this.langArr;
    });
    
  }

  SubmitProfile() {
    if (this.profileForm.invalid) {
      return;
    }
    const oldEmail = this.profileForm.controls.oldemail.value;
    const newEmail = this.profileForm.controls.email.value;
    //let isSubmit = false;

    if (oldEmail != newEmail) {
      //const that = this;
      let confirmMsg;
      confirmMsg = "Are you sure you want to change email ? If yes then you need to login again with new email id";
      let that  = this;
      this.alertService.confirmThis('Confirmation', confirmMsg, function () {
        
        that.updateProfile(that.profileForm.value,1);
      }, function () {
        console.log('No');
      });
    } else {
      
      this.updateProfile(this.profileForm.value,0);
    }

  }

  updateProfile(data,isLogout) {
    this.adminservice.updateProfile(data)
      .pipe(first())
      .subscribe((data: any) => {
        console.log(data);
        if (data.status) {
          this.isPasswordError = false;
          this.isPasswordSuccess = false;
          if (this.fileInput !== null) {
            const uploadData = new FormData();
            uploadData.append('image', this.fileInput, this.fileInput.name);
            this.adminservice.addImage(uploadData)
              .pipe(first())
              .subscribe((res: any) => {
                if (res.status) {
                  this.isSuccess = true;
                  this.successMessage = res.message;
                  this.isError = false;
                  this.currentUser$.profile_picture = res.data;
                  ///setTimeout(() => { this.UpdateSession(); }, 3000);
                  if(isLogout == 1){
                    this.logOut();
                  }else{
                    setTimeout(() => { this.UpdateSession(); }, 3000);
                  }
                } else {
                  this.isError = true;
                  this.isSuccess = false;
                  /*this.serverError$ = data.message.split('<br>').map(function (item) {
                    return item.trim();
                  });
                  this.errorMessage = this.serverError$;
                  */
                }
                this.fileInput = null;
                this.profileForm.get('image').reset();
              });
          } else {

            this.isSuccess = true;
            this.successMessage = data.message;
            this.isError = false;
            
             if(isLogout == 1){
               this.logOut();
             }else{
               setTimeout(() => { this.UpdateSession(); }, 3000);
             }

          }
          
        } else {
          this.isError = true;
          this.isSuccess = false;
          /*this.serverError$ = data.message.split('<br>').map(function (item) {
            return item.trim();
          });
          this.errorMessage = this.serverError$;
          */
        }
        if(data.message && data.message != ''){
          this.translate.get(data.message).subscribe((result: string) => {
            if(this.isSuccess == true){
              this.successMessage = result;
            }else{
              this.errorMessage = result;
            }
          }); 
        }


      });
  }


  SubmitChangePassword() {
    if (this.changePasswordForm.invalid) {
      return;
    }

    this.adminservice.changePassword(this.changePasswordForm.value)
      .pipe(first())
      .subscribe((data: any) => {
        this.isError = false;
        this.isSuccess = false;
        if (data.status) {
          this.isPasswordSuccess = true;
          //this.PasswordsuccessMessage = data.message;
          this.isPasswordError = false;
          this.changePasswordForm.reset();
        } else {
          this.isPasswordError = true;
          this.isPasswordSuccess = false;
          /*this.serverError$ = data.message.split('<br>').map(function (item) {
            return item.trim();
          });
          this.PassworderrorMessage = this.serverError$;
          */
        }
        if(data.message && data.message != ''){
          this.translate.get(data.message).subscribe((result: string) => {
            if(this.isPasswordSuccess == true){
              this.PasswordsuccessMessage = result;
            }else{
              this.PassworderrorMessage = result;  
            }
          });
        }

      });
  }

  UpdateSession() {
    this.currentUser$.company = this.profileForm.controls.company.value;
    this.currentUser$.language = this.profileForm.controls.language.value;
    this.storage.set('adminloginSession', this.currentUser$);
    // this.router.navigateByUrl('/admin/users', {skipLocationChange: true}).then(() =>
    // this.router.navigate(['/admin/settings']));

    window.location.reload();
  }

  logOut() {
    const currentUser = this.storage.get('adminloginSession');
    this.adminservice.logOut(currentUser)
      .pipe(first())
      .subscribe( (data: any) => {
        if (data.status) {
          this.storage.remove('adminloginSession');
          this.storage.remove('currentLanguage');
          this.router.navigate(['/admin/login']);
        }
      });
      
  }

}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../service/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { LocalstorageService } from '../service/localstorage.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.scss']
})
export class ResetpasswordComponent implements OnInit {

  resetpasswordForm: FormGroup;
  serverError$: any;
  invalidSetPassword = false;
  validSetPassword = false;
  timeLeft = 2;
  interval;
  isErrorrepwd = false;
  isErrorpwd = false;
  isError = false;
  errorMessage = '';
  isSuccess = false;
  successMessage = '';
  isLoading = false;
  isTokenValidate = false;

  constructor(
    private auth: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private storage: LocalstorageService,
    private spinner: NgxSpinnerService,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    if (this.auth.isLoggednIn()) {
      const currentUser = this.storage.get('loginSession');
      this.auth.logOut(currentUser);
      this.storage.remove('loginSession');
      this.storage.remove('isExpired');
    }
    this.resetpasswordForm = this.formBuilder.group({
      password: ['', [Validators.required, Validators.maxLength(25), Validators.minLength(6)]],
      confirmPassword: ['', [Validators.required, Validators.maxLength(25), Validators.minLength(6)]],
      resetPasswordToken: ['']
    }, { validator: this.passwordMatchValidator('password', 'confirmPassword') });

    this.route.paramMap.subscribe(params => {
      this.resetpasswordForm.controls['resetPasswordToken'].setValue(params.get('emailToken'));
      this.checkToken(params.get('emailToken'));
    });
  }

  checkToken(token) {
    var obj = { resetPasswordToken: token };
    this.auth.checkEmailToken(obj).pipe(first()).subscribe((data: any) => {
      if (data.status) {
        this.isTokenValidate = true;
      } else {

      }
    });
  }

  Validationpwd() {
    this.isErrorpwd = true;
  }
  ValidationErrorpwd() {
    this.isErrorpwd = false;
  }

  ValidationError() {
    this.isErrorrepwd = true;
  }
  ValidationErrorCheck() {
    this.isErrorrepwd = false;
  }
  passwordMatchValidator(password: string, confirmPassword: string) {
    return (group: FormGroup): { [key: string]: any } => {
      const f = group.controls[password];
      const t = group.controls[confirmPassword];
      if (f.value && t.value && f.value.length > 5 && t.value.length > 5 && f.value !== t.value) {
        if(this.translate.defaultLang === "de"){
        return {
          passwords: 'Passwort und Passwort best�tigen stimmen nicht �berein.'
        };
      }

      if(this.translate.defaultLang === "fr"){
        return {
          passwords: 'Mot de passe et Confirmer le mot de passe ne correspond pas.'
        };
      }
      
      else
      {
        return {
          passwords: 'Password and Confirm password does not matched.'
        };
      }
      }
      return {};
    };
  }

  submitSetPassword() {
    if (this.resetpasswordForm.invalid) {
      return;
    }
    //this.spinner.show();
    this.isLoading = true;
    const formvalues = this.resetpasswordForm.value;
    this.auth.setPassword(formvalues).pipe(first()).subscribe((data: any) => {
      if (data.status) {
        this.isSuccess = true;
        //this.successMessage = data.message;
        this.translate.get("reset_password_success").subscribe((result: string) => {
          this.successMessage = result;
        });
        this.isError = false;
        this.validSetPassword = true;
        this.invalidSetPassword = false;
        this.interval = setInterval(() => {
          if (this.timeLeft > 0) {
            this.timeLeft--;
          } else {
            clearInterval(this.interval);
            this.router.navigate(['login']);
          }
        }, 1000);
        //this.spinner.hide();
        this.isLoading = false;
      } else {
        this.isError = true;
        this.isSuccess = false;
        //this.errorMessage = data.message;
        this.serverError$ = data.message.split('<br>').map(function (item) {
          return item.trim();
        });

        this.translate.get("error_something_wrong").subscribe((result: string) => {
          this.errorMessage = result;
        });

        this.invalidSetPassword = true;
        this.validSetPassword = false;
        //this.spinner.hide();
        this.isLoading = false;
      }
    });
  }

}

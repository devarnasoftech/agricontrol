import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { AdminauthGuard } from './guards/adminauth.guard';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ConfirmEmailComponent } from './confirmemail/confirmemail.component';
import { RegisterFieldsComponent } from './registerfields/registerfields.component';
import { CropmasterComponent } from './cropmaster/cropmaster.component';
import { FaqComponent } from './faq/faq.component';
import { SettingComponent } from './setting/setting.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { GraphicalExportComponent } from './graphical-export/graphical-export.component';
import { AdminloginComponent } from './adminlogin/adminlogin.component';
import { AdminusersComponent } from './adminusers/adminusers.component';
import { AdmincropsComponent } from './admincrops/admincrops.component';
import { AdminprofileComponent } from './adminprofile/adminprofile.component';
import { MastersComponent } from './masters/masters.component';
import { LanguagefileComponent } from './languagefile/languagefile.component';
import { AdminlanguageComponent } from './adminlanguage/adminlanguage.component';
import { RedirectGuard } from './_helper/redirectGuard';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'register',
    component: RegistrationComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'confirmEmail/:emailToken',
    component: ConfirmEmailComponent
  },
  {
    path: 'registerFields',
    component: RegisterFieldsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'crops',
    component: CropmasterComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'master',
    component: MastersComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'faq',
    component: FaqComponent
  },
  {
    path: 'settings',
    component: SettingComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'graphicalExport',
    component: GraphicalExportComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'forgotpassword',
    component: ForgotpasswordComponent
  },
  {
    path: 'resetPassword/:emailToken',
    component: ResetpasswordComponent
  },
  {
    path: 'admin/login',
    component: AdminloginComponent
  },
  {
    path: 'admin/users',
    component: AdminusersComponent,
    canActivate: [AdminauthGuard]
  },
  {
    path: 'admin/settings',
    component: AdminprofileComponent,
    canActivate: [AdminauthGuard]
  },
  {
    path: 'admin/crops',
    component: AdmincropsComponent,
    canActivate: [AdminauthGuard]
  },
  {
    path: 'admin/manageLanguage',
    component: LanguagefileComponent,
    canActivate: [AdminauthGuard]
  },
  {
    path: 'admin/language',
    component: AdminlanguageComponent,
    canActivate: [AdminauthGuard]
  },
  {
    path: 'admin',
    redirectTo: "admin/login"
  },
  // {
  //   path:"**",
  //   canActivate: [RedirectGuard],
  //   component: RedirectGuard,
  //   data: {
  //     externalUrl: '/'
  //   }
  // }
  {
    path: '**',
    component: DashboardComponent,
    canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
  
}

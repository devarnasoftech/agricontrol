import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MastersService } from '../service/masters.service';
import { first } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { AlertService } from '../service/alert.service';
import { e } from '@angular/core/src/render3';

@Component({
  selector: 'app-masters',
  templateUrl: './masters.component.html',
  styleUrls: ['./masters.component.scss']
})
export class MastersComponent implements OnInit {
  
  mastersModal = 'none';
  machineForm: FormGroup;
  soilForm: FormGroup;
  pestsForm: FormGroup;
  fertilizerForm: FormGroup;
  pesticidesForm: FormGroup;
  isError = false;
  errorMessage = '';
  isSuccess = false;
  successMessage = '';

  allMachine$ : any;
  machineFormButton = 'Add';
  allSoil$ : any;
  soilFormButton = 'Add';
  allPests$ : any;
  pestsFormButton = 'Add';
  allFertilizer$: any;
  fertilizerButton = "Add";
  allPesticideType$: any;
  pesticideFormButtton = "Add";
  allPesticide$ : any;

  constructor(
    private formBuilder: FormBuilder,
    private master: MastersService,
    private translate: TranslateService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.machineForm = this.formBuilder.group({
      machine_name: ['', [Validators.required, Validators.maxLength(100)]],
      machine_id:[''],
      machine_old:['']
    });

    this.soilForm = this.formBuilder.group({
      soil_name:['',[Validators.required,Validators.maxLength(100)]],
      soil_id:[''],
      soil_old:['']
    });

    this.pestsForm = this.formBuilder.group({
      pests_name:['',[Validators.required,Validators.maxLength(100)]],
      pests_id:[''],
      pests_old:['']
    });

    this.fertilizerForm = this.formBuilder.group({
      fertilizer_name : ['',[Validators.required,Validators.maxLength(100)]],
      nitrogen : ['',[Validators.required,Validators.maxLength(3)]],
      phosphorus : ['',[Validators.required,Validators.maxLength(3)]],
      potassium_oxide : ['',[Validators.required,Validators.maxLength(3)]],
      magnesium : ['',[Validators.required,Validators.maxLength(3)]],
      fertilizer_id: ['']
    });

    this.pesticidesForm = this.formBuilder.group({
      pesticide_name: ['',[Validators.required,Validators.maxLength(100)]],
      type_id: ['',[Validators.required]],
      waiting_days: ['',[Validators.required,Validators.maxLength(3)]],
      pesticide_id:['']
    });

    this.getAllMachine();
    this.getAllSoil();
    this.getAllPests();
    this.getAllFertilizer();
    this.getAllPesticideType();
    this.getAllPesticide();
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  /* Start Machine */
  addMachine()
  {
    if (this.machineForm.invalid) {
      return;
    }
    const old_name = this.machineForm.value.machine_old;
    const new_name = this.machineForm.value.machine_name;
    
     if(old_name !== new_name){
      //console.log(this.machineForm.value);
      this.master.addMachine(this.machineForm.value).pipe(first()).subscribe((data : any)=>{
        ///console.log(data);
        if(data.status){
            this.isError = false;
            this.isSuccess = true;
            this.machineForm.reset();
            this.machineFormButton = "Add";
            this.getAllMachine();
        }else{
            this.isError = true;
            this.isSuccess = false;
        }
        if(data.message && data.message != ''){
            this.translate.get(data.message).subscribe((result: string) => {
              if(this.isSuccess == true){
                this.successMessage = result;
              }else{
                this.errorMessage = result;
              }
            });
        }
        ///console.log(this.successMessage);
      });
    }else{
      this.machineForm.reset();
      this.machineFormButton = 'Add';
    }
  }

  getAllMachine() {
    this.master.getAllMachine().pipe(first()).subscribe((data: any) => {
      this.allMachine$ = data['data'];
    });
  }

  editMachine(machine){
    this.machineForm.controls['machine_id'].setValue(machine.machine_id);
    this.machineForm.controls['machine_name'].setValue(machine.machine_name);
    this.machineForm.controls['machine_old'].setValue(machine.machine_name);
    this.machineFormButton = "Update";
  }

  deleteMachine(id){
    const that = this;
    let confirmMsg;
    confirmMsg = "Are you sure you want to delete this ?";
    that.alertService.confirmThis('Confirmation', confirmMsg, function () {
        //console.log('Yes');
        const deleteInput = { 'machine_id': id};
        that.master.deleteMachine(deleteInput).pipe(first()).subscribe((data: any) => {
          if(data.status){
            that.isError = false;
            that.isSuccess = true;
            that.getAllMachine();
          }else{
            that.isError = true;
            that.isSuccess = false;
          }
          if(data.message && data.message != ''){
            that.translate.get(data.message).subscribe((result: string) => {
              if(that.isSuccess == true){
                that.successMessage = result;
              }else{
                that.errorMessage = result;
              }
            });
          } 
        });
    }, function () {
      console.log('No');
    });

  }
  /* End Machine */

  /* Start Soil */
  actionSoilCultivation()
  {
     if(this.soilForm.invalid){
       return;
     }
     const old_name = this.soilForm.value.soil_old;
     const new_name = this.soilForm.value.soil_name;
    
     if(old_name !== new_name){
       ///console.log('new');
      this.master.actionSoil(this.soilForm.value).pipe().subscribe((data:any)=>{
          if(data.status){
              this.isError = false;
              this.isSuccess = true;
              this.soilForm.reset();
              this.soilFormButton = 'Add';
              this.getAllSoil();
          }else{
              this.isError = true;
              this.isSuccess = false;  
          }
          if(data.message && data.message != ''){
            this.translate.get(data.message).subscribe((result : string)=>{
                if(this.isSuccess == true){
                  this.successMessage = result;
                }else{
                  this.errorMessage = result;
                }
            });
          }
      });
    }else{
      this.soilForm.reset();
      this.soilFormButton = 'Add';
    }
  }

  getAllSoil(){
     this.master.getAllSoil().pipe(first()).subscribe((data:any)=>{
       this.allSoil$ = data['data'];
     });
  }

  editSoil(data){
    ///console.log(data);
    this.soilForm.controls['soil_name'].setValue(data.soil_name);
    this.soilForm.controls['soil_id'].setValue(data.soil_id);
    this.soilForm.controls['soil_old'].setValue(data.soil_name);
    this.soilFormButton = 'Update';
  }

  deleteSoil(id){
    const that = this;
    let msg;
    msg = "Are you sure you want to delete this ?";
    that.alertService.confirmThis('Confirmation',msg,function(){
      //console.log('yes');
      const input = {'soil_id':id};
      that.master.deleteSoil(input).pipe(first()).subscribe((data:any)=>{
         console.log(data);
         if(data.status){
           that.isError = false;
           that.isSuccess = true;
           that.getAllSoil();
         }else{
           that.isError = true;
           that.isSuccess = false;
         }
         if(data.message && data.message != ''){
           that.translate.get(data.message).subscribe((result:string)=>{
              if(that.isSuccess == true){
                 that.successMessage = result;
              }else{
                 that.errorMessage = result;
              }
           });
         }
      }); 
    },function(){
        console.log('No');
    });
  }
  /* End Soil */

  /* Start Pests */
  actionPests(){
    if(this.pestsForm.invalid){
      return;
    }
    ///console.log(this.pestsForm.value);
    const old_name = this.pestsForm.value.pests_old;
    const new_name = this.pestsForm.value.pests_name;

    if(old_name !== new_name){
       this.master.actionPests(this.pestsForm.value).pipe(first()).subscribe((data:any)=>{
           if(data.status){
              this.isError = false;
              this.isSuccess = true;
              this.pestsForm.reset();
              this.pestsFormButton = "Add"; 
              this.getAllPests();
           }else{
              this.isError = false;
              this.isSuccess = true;
           }
           if(data.message && data.message != ''){
              this.translate.get(data.message).subscribe((result:string)=>{
                 if(this.isSuccess == true){
                    this.successMessage = result;
                 }else{
                    this.errorMessage = result;
                 }
             });
           }
       });
    }else{
       this.pestsForm.reset();
       this.pestsFormButton = "Add";
    }
  }

  getAllPests(){
     this.master.getAllPests().pipe(first()).subscribe((data:any)=>{
       this.allPests$ = data['data'];
     });
  }

  editPests(data){
    console.log(data);
    this.pestsForm.controls['pests_id'].setValue(data.pests_id);
    this.pestsForm.controls['pests_name'].setValue(data.pests_name);
    this.pestsForm.controls['pests_old'].setValue(data.pests_name);
    this.pestsFormButton = "Update"; 
  }

  deletePests(id){
    let that = this;
    let msg = "Are you sure you want to delete this";

    that.alertService.confirmThis('Confirmation',msg,function(){
       const input = {'pests_id' : id}
       that.master.deletePests(input).pipe(first()).subscribe((data:any)=>{
          if(data.status){
            that.isError = false;
            that.isSuccess = true;
            that.getAllPests();
          }else{
            that.isError = true;
            that.isSuccess = false;
          }
          if(data.message && data.message != ''){
            that.translate.get(data.message).subscribe((result:string)=>{
              if(that.isSuccess == true){
                that.successMessage = result;
              }else{
                that.errorMessage = result;
              }
            });
          }
       });
    },function(){
       //console.log('no');
    });
  }
  /* End Pests */

  /* Start Fertilizer */
  actionFertilizer(){
    if(this.fertilizerForm.invalid){
      return;
    }
    console.log(this.fertilizerForm.value);
    this.master.actionFertilizer(this.fertilizerForm.value).pipe(first()).subscribe((data:any)=>{
       ///console.log(data);
       if(data.status){
         this.isError = false;
         this.isSuccess = true;
         this.fertilizerForm.reset();
         this.fertilizerButton = "Add";
         this.getAllFertilizer();
       }else{
         this.isError = true;
         this.isSuccess = false;
       }
       if(data.message && data.message != ''){
         this.translate.get(data.message).subscribe((result:string)=>{
           if(this.isSuccess == true){
             this.successMessage = result;
           }else{
             this.errorMessage = result;
           }
         });
       }
    });
  }

  getAllFertilizer(){
    this.master.getAllFertilizer().pipe(first()).subscribe((data:any)=>{
        this.allFertilizer$ = data['data']
    });
  }

  editFertilizer(data){
    //console.log(data);
    this.fertilizerForm.controls['fertilizer_id'].setValue(data.fertilizer_id);
    this.fertilizerForm.controls['fertilizer_name'].setValue(data.fertilizer_name);
    this.fertilizerForm.controls['nitrogen'].setValue(data.nitrogen);
    this.fertilizerForm.controls['phosphorus'].setValue(data.phosphorus);
    this.fertilizerForm.controls['potassium_oxide'].setValue(data.potassium_oxide);
    this.fertilizerForm.controls['magnesium'].setValue(data.magnesium);
    this.fertilizerButton = "Update";
  }

  deleteFertilizer(id){

    let that = this;
    let msg = "Are you sure you want to delete this ?";
    that.alertService.confirmThis('Confirmation',msg,function(){
      const input = {'fertilizer_id':id};
      that.master.deleteFertilizer(input).pipe((first())).subscribe((data:any)=>{
         if(data.status){
          that.isError = false;
          that.isSuccess = true;
          that.getAllFertilizer();
         }else{
          that.isError = true;
          that.isSuccess = false;
         }
         if(data.message && data.message != ''){
          that.translate.get(data.message).subscribe((result:string)=>{
             if(that.isSuccess == true){
              that.successMessage = result;
             }else{
              that.errorMessage = result;
             }
           });
         }
      });
    },function(){
      console.log('No');
    });
    

  }
  /* End Fertilizer */

  /* Start Pesticides */
  getAllPesticideType(){
    this.master.getAllPesticideType().pipe(first()).subscribe((data:any)=>{
        this.allPesticideType$ = data['data']
    });
  }

  actionPesticides(){
    if(this.pesticidesForm.invalid){
      return;
    }
    //console.log(this.pesticidesForm.value);
    this.master.actionPestiscide(this.pesticidesForm.value).pipe(first()).subscribe((data:any)=>{
       ///console.log(data);
       if(data.status){
         this.isError = false;
         this.isSuccess = true;
         this.pesticidesForm.reset();
         this.pesticideFormButtton = "Add";
         this.getAllPesticide();
         this.pesticidesForm.controls['type_id'].setValue('');
       }else{
         this.isError = true;
         this.isSuccess = false;
       }
       if(data.message && data.message != ''){
        this.translate.get(data.message).subscribe((result:string)=>{
          if(this.isSuccess == true){
            this.successMessage = result;
          }else{
            this.errorMessage = result;
          }
        });
      }
    });

  }

  getAllPesticide(){
    this.master.getAllPesticide().pipe(first()).subscribe((data:any)=>{
       this.allPesticide$ = data['data']
    });
  }

  editPesticide(data){
    console.log(data);
    this.pesticidesForm.controls['pesticide_id'].setValue(data.pesticide_id);
    this.pesticidesForm.controls['pesticide_name'].setValue(data.pesticide_name);
    this.pesticidesForm.controls['type_id'].setValue(data.type_id);
    this.pesticidesForm.controls['waiting_days'].setValue(data.waiting_days);
    this.pesticideFormButtton = "Update"; 
  }

  deletePesticide(id){
    let that = this;
    let msg = "Are you sure you want to delete this ?"
    that.alertService.confirmThis('Confirmation',msg,function(){
      const input = {'pesticide_id' : id};
      that.master.deletePestiscide(input).pipe(first()).subscribe((data:any)=>{
        ///console.log(data); 
        if(data.status){
           that.isError = false;
           that.isSuccess = true;
           that.getAllPesticide();
         }else{
           that.isError = true;
           that.isSuccess = false;
         }
         if(data.message && data.message != ''){
           that.translate.get(data.message).subscribe((result:string)=>{
             if(that.isSuccess == true){
               that.successMessage = result;
             }else{
               that.errorMessage = result;
             }
           });
         }
      });

    },function(){
      console.log('No');
    });
  }
  /* End Pesticides */

   openmasterModal() {
    this.mastersModal = 'block';
   }

   closemasterModal() {
     debugger;
     this.mastersModal = 'none';
   }

   FadeOutLink() {
    setTimeout( () => {
          this.isSuccess = false;
          this.isError = false;
        }, 2000);
   }

}

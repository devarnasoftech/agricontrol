import { Component, OnInit } from '@angular/core';
import { AuthService } from '../service/auth.service';
import { LocalstorageService } from '../service/localstorage.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import * as CryptoJS from 'crypto-js';
import { TranslateService } from '@ngx-translate/core';
import { DeletealertService } from '../service/deletealert.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  serverError$: any;
  invalidLogin = false;
  isError = false;
  isErrorCheck = false;
  isErrorpwd = false;
  isLoading = false;
  errorMessage = '';
  secretkey = 'agricontrol';
  constructor(
    private auth: AuthService,
    private router: Router,
    private formBuilder: FormBuilder,
    private storage: LocalstorageService,
    private spinner: NgxSpinnerService,
    private translate: TranslateService,
    private deletealertService: DeletealertService
  ) {
    if (this.auth.isLoggednIn()) {
      this.router.navigate(['dashboard']);
    }
  }

  ngOnInit() {

    const defaultLanguage =  this.storage.get('defaultLanguage');
    const rememberMeStatus = this.storage.get('rememberMe');

    this.storage.remove('loginSession');
    this.storage.remove('adminloginSession');
    this.storage.remove('currentLanguage');
    this.storage.remove('isExpired');
    localStorage.clear();
    sessionStorage.clear();
    this.storage.removeAll();

    this.storage.set("defaultLanguage", defaultLanguage);
    this.storage.set("rememberMe", rememberMeStatus);

    let email = '';
    let password = '';
    let rememberMe = '';
    if (rememberMeStatus && rememberMeStatus.rememberme) {
      email = rememberMeStatus.email;
      const passwordData = CryptoJS.AES.decrypt(rememberMeStatus.password, this.secretkey);
      password = passwordData.toString(CryptoJS.enc.Utf8);
      rememberMe = rememberMeStatus.rememberme;
    }
    this.loginForm = this.formBuilder.group({
      email: [email, [Validators.required, Validators.maxLength(50), Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
      password: [password, [Validators.required, Validators.maxLength(25), Validators.minLength(6)]],
      rememberme: [rememberMe]
    });
  }


  ValidationError() {
    this.isErrorCheck = true;
  }
  ValidationErrorCheck() {
    this.isErrorCheck = false;
  }

  Validationpwd() {
    this.isErrorpwd = true;
  }
  ValidationErrorpwd() {
    this.isErrorpwd = false;
  }

  submitLogin() {

    //  let test= CryptoJS.AES.encrypt('text', '123456').toString();
    //  console.log("test",test);
    // let test1 =CryptoJS.AES.decrypt(test, '123456');
    // let test2 = test1.toString(CryptoJS.enc.Utf8);
    // console.log("test2",test2);

    if (this.loginForm.invalid) {
      return;
    }
    //this.spinner.show();
    this.isLoading = true;
    const formvalues = this.loginForm.value;
    this.auth.login(formvalues).pipe(first()).subscribe((data: any) => {
      //debugger;
      if (data.status) {
        ///console.log(data);
        if(data.deleted_user_msg){
           ///console.log('show popup');
          const that = this;
          let confirmMsg;
          that.translate.get('account_restore_confirmation').subscribe((result: string) => {
            confirmMsg = result;
          });
          let yesText = 'yes_restore';
          let noText = 'no_restore';
          that.deletealertService.confirmThis('Confirmation', confirmMsg,yesText,noText, function () {
            const Input = { 'user_id': data.user_id};
            that.auth.restoreAccount(Input).pipe(first()).subscribe(( response: any) => {
              ///console.log(response);
              if (response.status) {
                that.submitLogin();
              }
            });
          }, function () {
            //console.log('No');
          });
        }else{
            this.isError = false;
            this.storage.set('loginSession', data.data);
            this.storage.set('isExpired', data.isExpired);
            if (data.data && data.data.languageKey) {
              this.translate.setDefaultLang(data.data.languageKey);
            } else {
              this.translate.setDefaultLang('en');
            }
            const isRemember: boolean = this.loginForm.get('rememberme').value;
            if (isRemember) {
              formvalues.password = CryptoJS.AES.encrypt(formvalues.password, this.secretkey).toString();
              this.storage.set('rememberMe', formvalues);
            } else {
              this.storage.remove('rememberMe');
            }
            this.router.navigate(['']);
            //this.spinner.hide();
            this.isLoading = false;
          }
      } else {
        this.isError = true;
        this.serverError$ = data.message;
        
        if(this.serverError$ != ''){
          this.translate.get(this.serverError$).subscribe((result: string) => {
            this.errorMessage = result;
          });
        }else {
          this.translate.get("error_something_wrong").subscribe((result: string) => {
            this.errorMessage = result;
          });
        }
        /*
        if (this.serverError$ == "User Is Not Verified") {
          this.translate.get("user_notverified").subscribe((result: string) => {
            this.errorMessage = result;
          });
        } else if (this.serverError$ == "User Credentials doesn't Match or user doesn't exist.") {
          this.translate.get("user_notexists").subscribe((result: string) => {
            this.errorMessage = result;
          });
        } else {
          this.translate.get("error_something_wrong").subscribe((result: string) => {
            this.errorMessage = result;
          });
        }*/
        //this.errorMessage = this.serverError$;
        this.invalidLogin = true;
        this.spinner.hide();
        //this.isLoading = false;
      }
    });
  }
}

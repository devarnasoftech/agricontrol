import { Component, OnInit } from '@angular/core';
import { CropmasterService } from '../service/cropmaster.service';
import { first } from 'rxjs/operators';
import { LocalstorageService } from '../service/localstorage.service';

@Component({
  selector: 'app-cropmaster',
  templateUrl: './cropmaster.component.html',
  styleUrls: ['./cropmaster.component.scss']
})
export class CropmasterComponent implements OnInit {

  allCrops$: [];
  serverError$: any;
  changeCropColorError = false;

  constructor(private cropmaster: CropmasterService,  private storage: LocalstorageService) { }

  ngOnInit() {
    this.getAllCrops();
  }

  getAllCrops() {
    let defaultLanguage;
    const currentUser: any = this.storage.get('loginSession');
    defaultLanguage = {'language': currentUser.language};
    this.cropmaster.getAllUserCrops(defaultLanguage).subscribe(data => 
      this.allCrops$ = data['data']
    );
  }

  colorChanged($event, cropId) {
    const color = $event;
    const input = { 'cropId': cropId, 'color': color };
    this.cropmaster.changeCropColor(input)
      .pipe(first())
      .subscribe( (data: any) => {
        if (!data.status) {
          this.serverError$ = data.message.split('<br>').map(function(item) {
            return item.trim();
          });
          this.changeCropColorError = true;
        } else {
          // this.getAllCrops();
        }
      });
  }

}

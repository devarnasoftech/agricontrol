import { Component, OnInit, HostListener, ElementRef, Renderer, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DashboardService } from '../service/dashboard.service';
import { CommonService } from '../service/common.service';
import { CropmasterService } from '../service/cropmaster.service';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { DatepickerOptions } from 'ng2-datepicker';
import * as enLocale from 'date-fns/locale/en';
import { LocalstorageService } from '../service/localstorage.service';
import { TitleCasePipe, DatePipe } from '@angular/common';

declare var jQuery: any;

@Component({
  selector: 'app-registerfields',
  templateUrl: './registerfields.component.html',
  styleUrls: ['./registerfields.component.scss']
})
export class RegisterFieldsComponent implements OnInit {

  @Input() value = '';
  @Output() dateChange = new EventEmitter();

  currentUser$: any;
  addFieldForm: FormGroup;
  fieldErrors$: any;
  invalidRegisterField = false;
  currentDate: string;

  addCultureForm: FormGroup;
  fieldCultureErrors$: any;
  invalidRegisterFieldCulture = false;
  allCrops$: any;
  fieldData: any;
  fieldcountData: any;
  fieldModal = 'none';
  cultureModal = 'none';
  helpModal = 'none';
  helpText$: any;
  helpTextHeading: string;
  showHideStartedGuide = false;
  showHideSwappedWith = false;
  showHideEchoSize = false;
  showRemainingSpace = false;
  remainingSpace = 0;
  cultureSizeIssue = false;
  ecoSizeIssue = false;

  isError = false;
  isErrorfieldname = false;
  isErrorfieldsize = false;
  errorMessage = '';
  isLoading = false;
  isSubmitted = false;


  constructor(
    private dashboardService: DashboardService,
    private common: CommonService,
    private router: Router,
    private formBuilder: FormBuilder,
    private cropmaster: CropmasterService,
    private spinner: NgxSpinnerService,
    private storage: LocalstorageService,
    private titlecasePipe: TitleCasePipe,
    public el: ElementRef,
    public renderer: Renderer
  ) { 
    this.router.navigate(['/dashboard']);
  }

  ngOnInit() {
    this.addFieldForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(50)]],
      size: ['', [Validators.required, Validators.maxLength(10), Validators.pattern('[0-9 ]*')]],
      echo: [''],
      echoSize: [''],
      address: ['', Validators.maxLength(300)],
      city: ['', [Validators.maxLength(100), Validators.pattern('[a-zA-Z ]+')]],
      zipcode: ['', [Validators.maxLength(10), Validators.pattern('[0-9 ]*')]],
      notes: ['', Validators.maxLength(500)],
    }, { validator: this.fieldCustomValidation() });

    this.addCultureForm = this.formBuilder.group({
      delicate: [''],
      dateFrom: ['', [Validators.required]],
      dateTo: ['', [Validators.required]],
      cultivatedArea: ['', [Validators.required, Validators.maxLength(10)]],
      fieldId: [''],
      isSwapped: [''],
      swappedWith: [''],
      plantVariety: ['', Validators.maxLength(500)],
      notes: ['', Validators.maxLength(500)]
    }, { validator: this.customValidation() });

    this.getAllCrops();
    this.getFieldCount();
    this.getServerDate();
    this.formControlValueChanged();
    this.currentUser$ = this.storage.get('loginSession');
  }

  renderDatePicker() {
    this.renderDatepicker('dateFrom', null);
    this.renderDatepicker('dateTo', this.currentDate);
  }

  updatedateTo() {
    const from = this.addCultureForm.controls.dateFrom.value;
    this.renderDatepicker("dateTo", from);
  }

  renderDatepicker(id, minDate) {
    jQuery("#" + id).datepicker("destroy");
    jQuery("#" + id).datepicker({
      firstDay: 1,
      dateFormat: 'mm.dd.yy',
      minDate: new Date(minDate),
      onSelect: (value) => {
        this.value = value;
        if (id == 'dateFrom') {
          this.addCultureForm.controls['dateFrom'].setValue(new DatePipe('en-US').transform(this.value, 'MM.dd.yyyy'));
          this.updatedateTo();
        } else {
          this.addCultureForm.controls['dateTo'].setValue(new DatePipe('en-US').transform(this.value, 'MM.dd.yyyy'));
        }
      }
    });
    jQuery("#" + id).datepicker("refresh");
  }

  Validationfieldname() {
    this.isErrorfieldname = true;
  }
  ValidationErrorfieldname() {
    this.isErrorfieldname = false;
    // this.isErrorpwd = false;
  }

  Validationfieldsize() {
    this.isErrorfieldsize = true;
  }
  ValidationErrorfieldsize() {
    this.isErrorfieldsize = false;
    // this.isErrorpwd = false;
  }

  formControlValueChanged() {
    const echoSizeControl = this.addFieldForm.get('echoSize');
    this.addFieldForm.get('echo').valueChanges.subscribe(
      (mode: string) => {
        if (mode === '1') {
          echoSizeControl.setValidators([Validators.maxLength(10), Validators.pattern('[0-9 ]*')]);
        } else if (mode === '0') {
          echoSizeControl.clearValidators();
        }
        echoSizeControl.updateValueAndValidity();
      });
  }

  customValidation() {
    return (group: FormGroup): { [key: string]: any } => {
      const from = group.controls['dateFrom'];
      const to = group.controls['dateTo'];
      const isSwapped = group.controls['isSwapped'];
      const swappedWith = group.controls['swappedWith'];

      if (from.value && to.value && from.value > to.value) {
        return {
          dates: 'Date From should be less than Date To.'
        };
      }
      if (isSwapped.value === true && swappedWith.value === '') {
        return {
          swappedWithRequired: 'SwappedWith is required.'
        };
      }
      return {};
    };
  }

  fieldCustomValidation() {
    return (group: FormGroup): { [key: string]: any } => {
      const isecho = group.controls['echo'];
      const echoSize = group.controls['echoSize'];

      if (isecho.value === true && echoSize.value === '') {
        return {
          echoSizeRequired: 'Eco Size is required.'
        };
      }
      return {};
    };
  }

  getServerDate() {
    this.common.getServerDate().pipe(first()).subscribe((data: any) => {
      if (data.status) {
        this.currentDate = data.data;
        this.renderDatePicker();
        //this.addCultureForm.controls['dateFrom'].setValue(new DatePipe('en-US').transform(this.currentDate, 'MM.dd.yyyy'));
        //this.addCultureForm.controls['dateTo'].setValue(new DatePipe('en-US').transform(this.currentDate, 'MM.dd.yyyy'));
      }
    });
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  onDateChange(date, id) {
    if (id == 'dateFrom') {
      this.addCultureForm.controls['dateFrom'].setValue(new DatePipe('en-US').transform(date, 'MM.dd.yyyy'));
      this.renderDatepicker('dateTo', date);
    } else {
      this.addCultureForm.controls['dateTo'].setValue(new DatePipe('en-US').transform(this.currentDate, 'MM.dd.yyyy'));
    }
  }

  getAllCrops() {
    let defaultLanguage;
    const currentUser: any = this.storage.get('loginSession');
    defaultLanguage = { 'language': currentUser.language };
    this.cropmaster.getAllCrops(defaultLanguage).subscribe(
      data => this.allCrops$ = data['data']
    );
  }

  getFieldCount() {
    const currentUser: any = this.storage.get('loginSession');
    const Input = { 'language': currentUser.language };
    this.dashboardService.getDashboardTopBoxesData(Input).pipe(first()).subscribe((data: any) => {
      if (data.status) {
        this.fieldcountData = parseInt(data['data'].field_data.field_count, 10);
      } else {
        this.fieldcountData = 0;
      }
    });
  }

  openFieldModal() {
    this.addFieldForm.reset();
    this.isError = false;
    this.fieldModal = 'block';
  }

  closeFieldModal() {
    this.isError = false;
    this.addFieldForm.reset();
    this.showHideEchoSize = false;
    this.fieldModal = 'none';
  }

  closeCultureModal() {
    this.isError = false;
    this.addCultureForm.reset();
    this.showHideSwappedWith = false;
    this.cultureModal = 'none';
  }

  addField() {
    if (this.addFieldForm.invalid) {
      return;
    }
    //this.spinner.show();
    this.isLoading = true;
    this.dashboardService.addField(this.addFieldForm.value).pipe(first()).subscribe((data: any) => {
      if (data.status) {
        this.ecoSizeIssue = false;
        this.isError = false;
        this.fieldData = data.data;
        this.fieldModal = 'none';
        this.cultureModal = 'block';
        this.addCultureForm.reset();
        // this.addCultureForm.controls['dateFrom'].setValue(this.currentDate);
        // this.addCultureForm.controls['dateTo'].setValue(this.currentDate);
        this.addCultureForm.controls['dateFrom'].setValue(new DatePipe('en-US').transform(this.currentDate, 'MM.dd.yyyy'));
        this.addCultureForm.controls['dateTo'].setValue(new DatePipe('en-US').transform(this.currentDate, 'MM.dd.yyyy'));
        //this.spinner.hide();
        this.isLoading = false;
      } else {
        if (data.sizeIssue) {
          this.ecoSizeIssue = true;
        } else {
          this.isError = true;
          this.ecoSizeIssue = false;
          this.fieldModal = 'block';
          this.cultureModal = 'none';
          this.invalidRegisterField = true;
          this.fieldErrors$ = data.message.split('<br>').map(function (item) {
            return item.trim();
          });
          this.errorMessage = this.fieldErrors$;
        }
        //this.spinner.hide();
        this.isLoading = false;
      }
    });
  }

  addCulture() {
    const from = this.addCultureForm.controls.dateFrom.value;
    const to = this.addCultureForm.controls.dateTo.value;
    if (this.addCultureForm.invalid) {
      this.addCultureForm.controls['dateFrom'].setValue(new DatePipe('en-US').transform(from, 'MM.dd.yyyy'));
      this.addCultureForm.controls['dateTo'].setValue(new DatePipe('en-US').transform(to, 'MM.dd.yyyy'));
      return;
    }

    if (this.fieldData) {
      this.addCultureForm.controls['fieldId'].setValue(this.fieldData.ac_fieldId);
    }

    //this.spinner.show();
    this.isLoading = true;
    this.addCultureForm.controls['dateFrom'].setValue(new DatePipe('en-US').transform(from, 'yyyy-MM-dd'));
    this.addCultureForm.controls['dateTo'].setValue(new DatePipe('en-US').transform(to, 'yyyy-MM-dd'));
    this.dashboardService.addFieldCulture(this.addCultureForm.value).pipe(first()).subscribe((data: any) => {
      if (data.status) {
        this.cultureSizeIssue = false;
        this.isError = false;
        this.addCultureForm.reset();
        this.invalidRegisterFieldCulture = false;
        this.showRemainingSpace = false;
        this.remainingSpace = 0;
        this.fieldModal = 'none';
        this.cultureModal = 'none';
        this.fieldData = '';
        //this.spinner.hide();
        this.isLoading = false;
        this.router.navigate(['dashboard']);
      } else {
        if (data.sizeIssue) {
          this.cultureSizeIssue = true;
        } else {
          this.cultureSizeIssue = false;
          this.isError = true;
          this.fieldModal = 'none';
          this.cultureModal = 'block';
          this.invalidRegisterFieldCulture = true;
          this.fieldCultureErrors$ = data.message.split('<br>').map(function (item) {
            return item.trim();
          });
          this.errorMessage = this.fieldCultureErrors$;
        }
        //this.spinner.hide();
        this.isLoading = false;
      }
      this.addCultureForm.controls['dateFrom'].setValue(new DatePipe('en-US').transform(from, 'MM.dd.yyyy'));
      this.addCultureForm.controls['dateTo'].setValue(new DatePipe('en-US').transform(to, 'MM.dd.yyyy'));
    });
  }

  openhelpModal(text: string) {
    this.helpTextHeading = this.titlecasePipe.transform(text);
    this.helpText$ = this.common.getHelpText(text);

    this.helpModal = 'block';
  }

  closehelpModal() {
    this.helpModal = 'none';
  }
  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    this.helpModal = 'none';
  }


  checkUncheckSwapping(event) {
    this.addCultureForm.controls['swappedWith'].setValue('');
    if (event.target.checked) {
      this.showHideSwappedWith = true;
    } else {
      this.showHideSwappedWith = false;
    }
  }

  checkUncheckEcho(event) {
    this.addFieldForm.get('echoSize').reset();
    if (event.target.checked) {
      this.showHideEchoSize = true;
    } else {
      this.showHideEchoSize = false;
    }
  }

  getFieldRemainingSpaceForDuration(input) {
    const from = this.addCultureForm.controls.dateFrom.value;
    const to = this.addCultureForm.controls.dateTo.value;
    const fieldId = this.fieldData != null ? this.fieldData.ac_fieldId : null;

    // if (input === 'from' && from !== null && this.lastFromDate != from) {
    //   this.lastFromDate = from;
    // }
    // if (input === 'to' && from !== null && this.lastToDate != to) {
    //   this.lastToDate = to;
    // }
    if (from !== null && to !== null && fieldId !== '' && from <= to) {
      this.getFieldRemainingSpace(from, to, fieldId);
    }
  }

  getFieldRemainingSpace(from, to, fieldId) {
    const InputData = { 'from': from, 'to': to, 'fieldId': fieldId };
    this.dashboardService.getFieldRemainingSpace(InputData)
      .pipe(first())
      .subscribe((data: any) => {
        if (data.status) {
          this.showRemainingSpace = true;
          this.remainingSpace = data.data;
        } else {
          this.showRemainingSpace = false;
          this.remainingSpace = 0;
        }
      });
  }

  closemodal(modalName) {
    const modal = document.getElementById(modalName + 'modalId');
    const modalContent = document.getElementById(modalName + 'ContentId');
    const that = this;
    window.onclick = function (event) {
      if (event.target === modal && event.target !== modalContent) {
        if (modalName === 'field') {
          that.closeFieldModal();
        }
        if (modalName === 'culture') {
          that.closeCultureModal();
          window.location.href = "/dashboard";
        }
        if (modalName === 'help') {
          that.closehelpModal();
        }
        modal.style.display = 'none';
      }
    };
  }

}

// function renderDatepicker(id, minDate) {
//   if (minDate == null) {
//     $("#" + id).datepicker({
//       dateFormat: 'dd.mm.yy',
//       onSelect: (value) => {
//         this.value = value;
//         this.dateChange.next(value);
//       }
//     });
//   } else {
//     $("#" + id).datepicker({
//       dateFormat: 'dd.mm.yy',
//       minDate: new Date(minDate),
//       onSelect: (value) => {
//         this.value = value;
//         this.dateChange.next(value);
//       }
//     });
//   }

// }

function change(obj) {
  var content = $(obj).find(".popover__content");
  if ($(content).hasClass('bottom_set')) {
    $(content).removeClass('bottom_set');
  }
  if ($(content).offset() != undefined) {
    if (($(content).offset().top + $(content).height()) >= $(window).height()) {
      $(content).addClass('bottom_set');
    }
  }
}

function setModelValue(crop_name, culture_size, duration, start_date, end_date, before,
  beforeJson, swapped_with_text, swappedWith,
  crop_rotation_rule_violated, rule_breaked, beforeCulture) {
  debugger;
  if (crop_name == undefined || crop_name == '') {
    return;
  }
  document.getElementById("beforeDiv").style.display = 'block';
  document.getElementById("swappedwithDiv").style.display = 'block';
  document.getElementById("rulebreakedDiv").style.display = 'block';
  document.getElementById("modelCropName").innerText = crop_name;
  document.getElementById("modelFieldSize").innerText = culture_size;
  document.getElementById("modelduration").innerText = duration;
  document.getElementById("modeldurationdate").innerText = start_date + ' - ' + end_date;
  document.getElementById("modelduration").innerText = duration;
  document.getElementById("swapped_with_text").innerText = swapped_with_text;
  document.getElementById("swapped_with_name").innerText = swappedWith;
  document.getElementById("crop_rotation_rule_violated").innerText = crop_rotation_rule_violated;
  var beforeHTML = '';
  document.getElementById("beforeDiv").innerHTML = beforeHTML;
  for (let index = 0; index < beforeJson.length; index++) {
    if (index == 0) {
      beforeHTML += '<span>' + before + '</span> : ';
    }
    beforeHTML += '<span>' + beforeJson[index].size + '</span>a, ';
    beforeHTML += '<span>' + beforeJson[index].start_date + '</span> - <span>' + beforeJson[index].end_date + '</span><br />';
  }
  document.getElementById("beforeDiv").innerHTML = beforeHTML;

  if (swappedWith == '') {
    document.getElementById("swappedwithDiv").style.display = 'none';
  }
  if (rule_breaked == '0') {
    document.getElementById("rulebreakedDiv").style.display = 'none';
  }
  $("#hoverInfoModel").modal('show');
}



var session = localStorage.getItem("loginSession");
if (session != undefined && session != null) {
  var sessionJson = JSON.parse(session);
  if (sessionJson != undefined && sessionJson != null && sessionJson.languageKey) {
    $("html").attr("lang", sessionJson.languageKey);
  }
}

// (function (factory) {
//   if (typeof define === "function" && define.amd) {
//     define(["../widgets/datepicker"], factory);
//   } else {
//     factory(jQuery.datepicker);
//   }
// }(function (datepicker) {
//   datepicker.regional.fr = {
//     closeText: "Fermer",
//     prevText: "Précédent",
//     nextText: "Suivant",
//     currentText: "Aujourd'hui",
//     monthNames: ["janvier", "février", "mars", "avril", "mai", "juin",
//       "juillet", "août", "septembre", "octobre", "novembre", "décembre"],
//     monthNamesShort: ["janv.", "févr.", "mars", "avr.", "mai", "juin",
//       "juil.", "août", "sept.", "oct.", "nov.", "déc."],
//     dayNames: ["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"],
//     dayNamesShort: ["dim.", "lun.", "mar.", "mer.", "jeu.", "ven.", "sam."],
//     dayNamesMin: ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"],
//     weekHeader: "Sem.",
//     dateFormat: "dd.mm.yy",
//     firstDay: 1,
//     isRTL: false,
//     showMonthAfterYear: false,
//     yearSuffix: "",
//     minDate: null,
//     id: null,
//     onSelect: (value) => {
//       debugger;
//       this.value = value;
//       if (this.id == 'dateFrom') {
//         this.addCultureForm.controls['dateFrom'].setValue(this.value);
//         this.updatedateTo();
//       } else if (this.id == "startDate") {
//         this.addCultureForm.controls['startDate'].setValue(this.value);
//         this.updateendDate();
//       } else if (this.id == "endDate") {
//         this.addCultureForm.controls['endDate'].setValue(this.value);
//       } else {
//         this.addCultureForm.controls['dateTo'].setValue(this.value);
//       }
//     },
//     beforeShow: function () {
//       debugger;
//       if (this.id == "dateFrom") {
//         $("#modalBodyCulture").scrollTop(10);
//       } else {
//         $("#modalBodyCulture").scrollTop(95);
//       }
//     }
//   };
//   return datepicker.regional.fr;
// }));

// (function (factory) {
//   if (typeof define === "function" && define.amd) {
//     define(["../widgets/datepicker"], factory);
//   } else {
//     factory(jQuery.datepicker);
//   }
// }(function (datepicker) {
//   datepicker.regional.de = {
//     closeText: "Erledigt",
//     prevText: "früher",
//     nextText: "Nächster",
//     currentText: "heute",
//     monthNames: ["Januar", "Februar", "März", "April", "Mai", "Juni",
//       "Juli", "August", "September", "Oktober", "November", "Dezember"],
//     monthNamesShort: ["Jan", "Feb", "Mrz", "Apr", "Mai", "Jun",
//       "Jul", "Aug", "Sept", "Okt", "Nov", "Dez"],
//     dayNames: ["Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"],
//     dayNamesShort: ["Son", "Mon", "Die", "Mit", "Don", "Fre", "Sam"],
//     dayNamesMin: ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"],
//     weekHeader: "Sem.",
//     dateFormat: "dd.mm.yy",
//     firstDay: 1,
//     isRTL: false,
//     showMonthAfterYear: false,
//     yearSuffix: "",
//     minDate: null,
//     id: null,
//     onSelect: (value) => {
//       debugger;
//       this.value = value;
//       if (this.id == 'dateFrom') {
//         this.addCultureForm.controls['dateFrom'].setValue(this.value);
//         this.updatedateTo();
//       } else if (this.id == "startDate") {
//         this.addCultureForm.controls['startDate'].setValue(this.value);
//         this.updateendDate();
//       } else if (this.id == "endDate") {
//         this.addCultureForm.controls['endDate'].setValue(this.value);
//       } else {
//         this.addCultureForm.controls['dateTo'].setValue(this.value);
//       }
//     },
//     beforeShow: function () {
//       debugger;
//       if (this.id == "dateFrom") {
//         $("#modalBodyCulture").scrollTop(10);
//       } else {
//         $("#modalBodyCulture").scrollTop(95);
//       }
//     }
//   };
//   return datepicker.regional.de;
// }));